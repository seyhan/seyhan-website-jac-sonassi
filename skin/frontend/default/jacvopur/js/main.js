
jQuery( document ).ready(function(jQuery) {

    var ageConfirmContent = jQuery('#opening-box');
    if(ageConfirmContent.length !== 0) {
        jQuery.magnificPopup.open({
            items: {
                src: jQuery('#opening-box'),
                type: 'inline'
            },
            closeOnBgClick: false,
            showCloseBtn: false
        });
    }
		
	jQuery( '#mobile-menu' ).click(function(e) {
		e.preventDefault();
		jQuery( '#mainmenu-container' ).slideToggle();
		jQuery( '#search_mini_form' ).removeClass('mobile-active');
		jQuery( '#cart-block' ).removeClass('active');
	});

	jQuery( '#mobile-search' ).click(function(e) {
		e.preventDefault();
		jQuery( '#search_mini_form' ).toggleClass('mobile-active');
		jQuery( '#mainmenu-container' ).hide();
		jQuery( '#cart-block ' ).removeClass('active');
	});

	jQuery( '#mobile-cart' ).click(function(e) {
		e.preventDefault();
		jQuery( '#cart-block' ).toggleClass('active');
		jQuery( '#mainmenu-container' ).hide();
		jQuery( '#search_mini_form' ).removeClass('mobile-active');
	});

	jQuery( '#cart, #cart-close-link' ).click(function(e) {
		e.preventDefault();
		jQuery( '#cart-block, #cart' ).toggleClass('active');
		jQuery( '#mainmenu-container' ).hide();
		jQuery( '#search_mini_form' ).removeClass('mobile-active');
	});

	jQuery('.currency-locale-link').click( function(e) {
		e.preventDefault();
		jQuery('.localisation-menu').slideToggle();
	});

	jQuery('#localisation-container').mouseleave(function(e) {
		if (e.target.nodeName !== 'SELECT') {
			jQuery('.localisation-menu').slideUp();
		}
	});

	jQuery('.changeCountryStoreLink').click(function(){
		setCookie('store_country', jQuery(this).data('country-code'), 1825);
	});

	jQuery( '#country-link-footer' ).click(function(e) {
		e.preventDefault();
		jQuery( '#country-block-footer' ).slideToggle();
	});

	jQuery( '#currency-link-footer' ).click(function(e) {
		e.preventDefault();
		jQuery( '#currency-block-footer' ).slideToggle();
	});

	if (jQuery('.column-one-quarter').css('float') == 'none') {
		jQuery( '.more-info-link' ).click(function(e) {
			e.preventDefault();
			jQuery(this).next( 'div.more-info-content' ).slideToggle();
		});
	}
	else {
		jQuery('.more-info-link').magnificPopup({
		  type: 'inline',
		  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
		});
	}

	jQuery( '#category-view-all-products-link, #category-go-back-link' ).click(function(e) {
		e.preventDefault();
		jQuery( '#category-list' ).slideToggle();
		jQuery( '#product-list' ).slideToggle();
		jQuery( '#category-choice-text' ).slideToggle();
		jQuery( '#category-go-back-text' ).slideToggle();
	});
	
	jQuery('#mobile-filter-menu').click(function(e) {
		e.preventDefault();
		jQuery('#product-filter-options').slideToggle();
	});

	jQuery('#product-filter-options h5').click(function(e) {
		e.preventDefault();
		jQuery(this).next('ul').slideToggle();
	});

	jQuery('.wp-sidebar h5').click(function(e) {
		e.preventDefault();
		jQuery(this).next('div').find('ul').slideToggle();
	});
	
	jQuery('.product-more-info-link').click(function(e) {
		e.preventDefault();
		jQuery(this).toggleClass('active');
		jQuery(this).next('div').slideToggle();
	});
	
	var tabs = jQuery( "#tabs" );
	if (tabs) tabs.tabs( { active: 0 } );

	jQuery('.hiddenText').after('<a href="#" class="readMore">Read more</a>')
	jQuery('.hiddenText').prev( 'p' ).addClass( 'inlineText' );
 
	jQuery('a.readMore').click(function(){
		jQuery(this).prev().slideToggle(function() {
			jQuery(this).next('a.readMore').text(function (index, text) {
				return (text == 'Read more' ? 'Read less' : 'Read more');
			});
		});
		return false;
	});

	jQuery( "#tabs-1" ).wrapInner( "<div class='product-description-content'></div>");
	jQuery('.product-description-content').readmore({
		speed: 150,
		maxHeight: 200
	});
});

function setCookie(name, value, days) {
	var date = new Date();
	date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
	var expiryDate = date.toGMTString();
	document.cookie = name + "=" + value + ";expires=" + expiryDate + "; path=/";
}
