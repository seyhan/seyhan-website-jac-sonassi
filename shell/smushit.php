<?php
/**
 * @category    Fishpig
 * @package    Fishpig_SmushIt
 * @license      http://fishpig.co.uk/license.txt
 * @author       Ben Tideswell <ben@fishpig.co.uk>
 */

	$abstract = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'abstract.php';
	
	if (!is_file($abstract)) {
		return;
	}

	require_once $abstract;
	
	/**
	 * To allow calling via the browser, set FISHPIG_SMUSHIT_DEBUG to true
	 *
	 * @const bool
	 */
	define('FISHPIG_SMUSHIT_DEBUG', false);
	
	/**
	 * Default number of images to be smushed if limit parameter is not set
	 *
	 * @const int
	 */
	define('FISHPIG_SMUSHIT_LIMIT_DEFAULT', 5);

	if (FISHPIG_SMUSHIT_DEBUG) {
		ini_set('display_errors', 1);
		error_reporting(E_ALL);
	}

	class Fishpig_Shell_SmushIt extends Mage_Shell_Abstract
	{
		/**
		 * If calling via the browser, get args differently
		 *
		 * @return $this
		 */
		protected function _parseArgs()
		{
			if (FISHPIG_SMUSHIT_DEBUG && $_SERVER['REQUEST_METHOD'] === 'GET') {
				$this->_args = $_GET;
			}
			else {
				parent::_parseArgs();
			}
			
			return $this;
		}
		
		/**
		 * Allow calling via the browser for testing
		 *
		 * @return bool
		 */
	    protected function _validate()
	    {
	    	return FISHPIG_SMUSHIT_DEBUG
	    		? true
	    		: parent::_validate();
	    }
    
		/**
		 * Retrieve Usage Help Message
		 *
		*/
		public function usageHelp()
		{
			return <<<USAGE

	Usage:  php -f shell.php -- -limit 5


USAGE;
		}
    
		/**
		 * Perform the smushing
		 *
		 * @return $this
		 */
		public function run()
		{
			$limit = (int)$this->getArg('limit');

			if ($limit === 0) {
				$limit = FISHPIG_SMUSHIT_LIMIT_DEFAULT;
			}

			Mage::helper('smushit')->autoSmushImages($limit);
			
			return $this;
		}
	}

	/**
	 * Create the object and call the run method
	 *
	 */
	$shell = new Fishpig_Shell_SmushIt();
	$shell->run();
	