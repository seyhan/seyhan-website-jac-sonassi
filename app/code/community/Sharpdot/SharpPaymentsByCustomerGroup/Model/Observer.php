<?php
/**
 * Magento
 *
 *
 * @category    Sharpdot
 * @package     Sharpdot_SharpPaymentsByCustomerGroup
 * @copyright   Copyright (c) 2010 Sharpdot Inc. (http://www.sharpdotinc.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 *
 * @category   Sharpdot
 * @package    Sharpdot_SharpPaymentsByCustomerGroup
 * @author     Mike D
 */
class Sharpdot_SharpPaymentsByCustomerGroup_Model_Observer
{
	public function payment_method_is_active($observer)
	{
		if(!Mage::getStoreConfig('sharppaymentsbycustomergroup/settings/enabled')){
			return $this;
		}
		
		$event = $observer->getEvent();
		$paymentCode = $event->getMethodInstance()->getCode();
		$paymentAvaliable = $event->result->isAvailable;

		if($paymentAvaliable){			
			//Check if we are allowing it for the customer group
			$customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();			
			$allowedCustomerGroupIds = Mage::getStoreConfig('sharppaymentsbycustomergroup/payment_methods/'.$paymentCode);
			
			if(strlen($allowedCustomerGroupIds) !==0){	//Only Continue if not empty. NOTE: could not use empty() because $allowedCustomerGroupIds can be 0. for that reason we cant use empty. 
				$allowedCustomerGroupArray = explode(',', $allowedCustomerGroupIds);					
				if(!in_array($customerGroupId, $allowedCustomerGroupArray)){					
					$event->result->isAvailable = false;
				}
			}
		}		
		return $this;
	}
}