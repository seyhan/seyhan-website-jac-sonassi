<?php
/**
 * Morningtime LatestReviews extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Morningtime
 * @package    Morningtime_LatestReviews
 * @copyright  Copyright (c) 2009 Morningtime Internet, http://www.morningtime.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Morningtime_LatestReviews_Model_Config_Source_LinkType
{
    public function toOptionArray()
    {
        return array(
            array('value'=>'product', 'label'=>Mage::helper('adminhtml')->__('Product page')),
            array('value'=>'listing', 'label'=>Mage::helper('adminhtml')->__('Product page with ratings')),
            array('value'=>'review', 'label'=>Mage::helper('adminhtml')->__('Review detail page')),
            array('value'=>'section', 'label'=>Mage::helper('adminhtml')->__('Reviews section at product page')),
        );
    }
}
