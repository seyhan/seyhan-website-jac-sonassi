<?php
class Fooman_Common_Model_Selftester extends Mage_Core_Model_Abstract
{
    public $messages = array();
    public $errorOccurred = false;

    public function main ()
    {
        $this->messages[] = 'Starting ' . get_class($this);
        $failed = false;
        try {
            if (!$this->selfCheckLocation()) {
                $failed = true;
            }
            if (!$this->checkFileLocations()) {
                $failed = true;
            }
            if (!$this->magentoRewrites()) {
                $failed = true;
            }
            if (!$this->dbCheck()) {
                $failed = true;
            }
            if (!$this->getSettings()) {
                $failed = true;
            }
            if (!$failed) {
                $this->messages[] = 'Result: success';
            } else {
                $this->messages[] = 'Result: failure';
                $this->errorOccurred = true;
            }
        } catch (Exception $e) {
            $this->errorOccurred = true;
            $this->messages[] = $e->getMessage();
        }
        $this->messages[] = 'Self-test finished';
        return $this;
    }

    public function selfCheckLocation()
    {
        if (file_exists('app' . DIRECTORY_SEPARATOR . 'Mage.php')) {
            require_once 'app' . DIRECTORY_SEPARATOR . 'Mage.php';
            Mage::app();
            $this->messages[] = "Default store loaded";
            $this->_getVersions();
        } else {
            $this->messages[] = 'Can\'t instantiate Magento. Is the file uploaded to your root Magento folder?';
            throw new Exception();
            return false;
        }
        return true;
    }
    public function checkFileLocations()
    {
        $returnVal = true;
        $this->messages[] = "Checking file locations";
        foreach ($this->_getFiles() as $currentRow) {

            if (empty($currentRow)) {
                continue;
            }
            try {
                if (!file_exists($currentRow)) {
                    throw new Exception('File ' . $currentRow . ' does not exist');
                }
                if (!is_readable($currentRow)) {
                    throw new Exception(
                        'Can\'t read file ' . $currentRow . ' - please check file permissions and file owner.'
                    );
                }

                $handleExtFile = fopen($currentRow, "r");
                if (!$handleExtFile) {
                    throw new Exception(
                        'Can\'t read file contents ' . $currentRow
                        . ' - please check if the file got corrupted in the upload process.'
                    );
                }
                fclose($handleExtFile);
            } catch (Exception $e) {
                $this->messages[] = $e->getMessage();
                $returnVal = false;
            }
        }
        return $returnVal;
    }

    public function magentoRewrites ()
    {
        $returnVal = true;
        $this->messages[] = "Checking rewrites";

        foreach ($this->_getRewrites() as $currentRow) {

            if (empty($currentRow) || !$currentRow) {
                continue;
            }
            try {
                $this->_testRewriteRow($currentRow);
            } catch (Exception $e) {
                $this->messages[] = $e->getMessage();
                $returnVal = false;
            }
        }
        return $returnVal;
    }

    public function dbCheck ()
    {
        $dbOk = true;
        $localError = false;
        $this->messages[] = "Checking database";
        $installer = new Mage_Sales_Model_Mysql4_Setup('sales_setup');
        $installer->startSetup();

        if (isset ($_GET['fix'])) {
            $fix = $_GET['fix'] == 'true';
        } else {
            $fix =false;
        }

        foreach ($this->_getDbFields() as $field) {
            switch ($field[0]) {
                case 'eav':
                    try {
                        $sql = "SELECT `attribute_id` FROM `{$installer->getTable('eav_attribute')}` WHERE `attribute_code` ='$field[2]'";
                        $test = $installer->getConnection('core_read')->fetchRow($sql);
                        if (!isset($test['attribute_id']) && !$test['attribute_id'] > 0) {
                            throw new Exception('eav attribute ' . $field[2] . ' is not installed');
                            $localError = true;
                        }
                        $this->messages[] = "[OK] eav attribute " . $field[2]." with id ".$test['attribute_id']."";
                    } catch (Exception $e) {
                        if ($fix) {
                            $this->messages[] = "Attempting fix for eav attribute " . $field[2]."";
                            try {
                                $installer->addAttribute($field[1], $field[2], $field[3]);
                                $this->messages[] = "[FIX OK] eav attribute " . $field[2]." fixed";
                            } catch (Exception $e) {
                                $this->messages[] = "[FAILED] fixing eav attribute " . $field[2]."";
                                $dbOk = false;
                                $this->messages[] = $e->getMessage();
                                $localError = true;
                            }
                        } else {
                            $this->messages[] = "[FAILED] eav attribute " . $field[2] . "";
                            Mage::getSingleton('core/session')->addWarning('[FAILED] eav attribute ' . $field[2]);
                            $dbOk = false;
                            $this->messages[] = "[ERR] ".$e->getMessage();
                            $localError = true;
                        }
                    }
                    break;
                case 'sql-column':
                    try {
                        $test = $installer->run(
                            "SELECT `$field[2]` FROM `{$installer->getTable($field[1])}` LIMIT 0"
                        );
                        $this->messages[] = "[OK] column " . $field[2]."";
                    } catch (Exception $e) {
                        if ($fix) {
                            $this->messages[] = "Attempting fix for column " . $field[2]."";
                            try {
                                $test = $installer->run(
                                    "ALTER TABLE `{$installer->getTable($field[1])}` ADD COLUMN `$field[2]` $field[3]"
                                );
                                $this->messages[] = "[FIX OK] column " . $field[2]." fixed";
                            } catch (Exception $e) {
                                $this->messages[] = "[FAILED] fixing column " . $field[2]."";
                                $dbOk = false;
                                $this->messages[] = $e->getMessage();
                                $localError = true;
                            }
                        } else {
                            $this->messages[] = "[FAILED] column " . $field[2]."";
                            $dbOk = false;
                            $this->messages[] = "[ERR] ".$e->getMessage();
                            $localError = true;
                        }
                    }
                    break;
            }
        }
        $installer->endSetup();
        if (empty($localError)) {
            return true;
        } else {
            if ($dbOk == false) {
                $this->messages[]
                    = "<p>The selftest has found some problems with your database install.
                    You can attempt to fix this by appending ?fix=true to the address or clicking this <a href=\""
                    . htmlentities($_SERVER['PHP_SELF'])
                    . "?fix=true\">link</a>.</p><p style=\"color:red;\"><em>A DATABASE BACKUP IS strongly RECOMMENDED BEFORE ATTEMPTING THIS!</em></p>";
            }
            return false;
        }
    }

    protected function _testRewriteRow($currentRow)
    {
        switch ($currentRow[0]) {
        case 'resource-model':
            $model = Mage::getResourceModel($currentRow[1]);
            if (get_class($model) != $currentRow[2]) {
                throw new Exception(
                    'Trying to load class ' . $currentRow[2] . 'returns ' . get_class($model)
                    . '. Please refresh your Magento configuration cache and check if you have any conflicting extensions installed.'
                );
            }
            break;

        case 'model':
            $model = Mage::getModel($currentRow[1]);
            if (!($model instanceof $currentRow[2])) {
                throw new Exception(
                    'Trying to load class ' . $currentRow[2] . ' returns ' . get_class($model)
                    . '. Please refresh your Magento configuration cache and check if you have any conflicting extensions installed.'
                );
            }
            if (get_class($model) != $currentRow[2]) {
                throw new Exception(
                    'Trying to load class ' . $currentRow[2] . ' returns correct instance but unexpected class '. get_class($model)
                    . '. Please refresh your Magento configuration cache and check if you have any conflicting extensions installed.'
                );
            }
            break;
        case 'block':
            $block = Mage::app()->getLayout()->createBlock($currentRow[1]);
            if (!($block instanceof $currentRow[2])) {
                throw new Exception(
                    'Trying to load block ' . $currentRow[2] . ' returns ' . get_class($block)
                    . '. Please refresh your Magento configuration cache and check if you have any conflicting extensions installed.'
                );
            }
            if (get_class($block) != $currentRow[2]) {
                throw new Exception(
                    'Trying to load block ' . $currentRow[2] . ' returns correct instance but unexpected class ' . get_class($block)
                    . '. Please refresh your Magento configuration cache and check if you have any conflicting extensions installed.'
                );
            }
            break;
        }
    }

    public function getSettings()
    {
        foreach ($this->_getSettings() as $table => $tableValues) {

            $this->messages[] = $table;
            foreach ($tableValues as $setting) {
                $msg = array();
                foreach ($setting as $key => $value) {
                    $msg[] = $key . ': ' . $value;
                }
                $this->messages[] = implode(' | ', $msg);
            }

        }
        return true;
    }

    public function _getVersions()
    {
        $this->messages[] = "Magento version: " . Mage::getVersion();
    }

    public function _getDbFields()
    {
        return array();
    }

    public function _getRewrites ()
    {
        return array();
    }

    public function _getFiles ()
    {
        return array();
    }

    public function _getSettings()
    {
        return array();
    }

}
