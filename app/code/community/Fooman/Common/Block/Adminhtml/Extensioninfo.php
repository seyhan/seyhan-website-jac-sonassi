<?php

class Fooman_Common_Block_Adminhtml_Extensioninfo extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected $_idString = '';
    protected $_moduleName = '';

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setTemplate('fooman/common/selftester.phtml');
        $this->setShowSelftestButton(FALSE);
        if (Mage::getModel($this->_idString . '/selftester')) {
            $this->setShowSelftestButton(TRUE);
            $this->setSelftestButtonUrl(
                Mage::helper('adminhtml')->getUrl(
                    'adminhtml/selftester', array(
                        'module'     => $this->_idString,
                        'moduleName' => $this->_moduleName
                    )
                )
            );
        }
        $this->setConfigVersion((string)Mage::getConfig()->getModuleConfig($this->_moduleName)->version);

        return $this->_toHtml();
    }
}