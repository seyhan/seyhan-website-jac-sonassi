<?php

class GetCommerce_Stockin_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @return string
     */
    public function getUploadPath()
    {
        return Mage::getBaseDir('var') . DS . 'getcommerce_stockin';
    }
}