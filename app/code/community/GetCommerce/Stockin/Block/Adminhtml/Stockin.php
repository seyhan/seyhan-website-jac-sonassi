<?php

/**
 * Class GetCommerce_Promoblocks_Block_Adminhtml_Promoblock
 */
class GetCommerce_Stockin_Block_Adminhtml_Stockin extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $helper = Mage::helper('getcommerce_stockin');
        $this->_controller = 'adminhtml_stockin';
        $this->_blockGroup = 'getcommerce_stockin';
        $this->_headerText = $helper->__('Manage Stock Updates');
        $this->_addButtonLabel = $helper->__('Add Stock Update');
        parent::__construct();
    }
}