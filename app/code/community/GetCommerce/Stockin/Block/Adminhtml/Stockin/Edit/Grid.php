<?php

class GetCommerce_Stockin_Block_Adminhtml_Stockin_Edit_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    const HELPER = 'getcommerce_stockin';
    protected $_helper;

    public function __construct()
    {
        $this->_helper = Mage::helper(self::HELPER);
        parent::__construct();
        $this->setId('getcommerce_stockin');
        $this->setDefaultSort('stockin_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
        $this->setDefaultLimit('500');
    }

    protected function _prepareCollection()
    {
        $model = Mage::registry('getcommerce_stockin');
        $collection = $model->getImportProductCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('sku', array(
            'header'    => $this->_helper->__('SKU'),
            'align'     =>'left',
            'index'     => 'sku',
            'filter' => false,
        ));

        $this->addColumn('update_stock_qty_by', array(
            'header'    => $this->_helper->__('Update QTY By'),
            'align'     =>'left',
            'index'     => 'update_stock_qty_by',
            'filter' => false,
        ));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return '';
    }

}