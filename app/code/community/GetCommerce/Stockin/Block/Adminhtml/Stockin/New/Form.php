<?php

class GetCommerce_Stockin_Block_Adminhtml_Stockin_New_Form extends Mage_Adminhtml_Block_Widget_Form
{
    const HELPER = 'getcommerce_stockin';

    protected function _prepareForm()
    {
        $helper = Mage::helper(self::HELPER);
        $form = new Varien_Data_Form(array(
            'id'      => 'edit_form',
            'action'  => $this->getUrl('*/*/save'),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset(
            'fieldset_info', array(
                'legend' => $helper->__('Information'))
        );

        $fieldset->addField('stockin_data_file', 'file', array(
            'name'     => 'stockin_data_file',
            'label'    => Mage::helper('importexport')->__('Select File to Import'),
            'title'    => Mage::helper('importexport')->__('Select File to Import'),
            'required' => true
        ));

        $data = array();

        if (Mage::getSingleton('adminhtml/session')->getBannerData()) {
            $data = Mage::getSingleton('adminhtml/session')->getBannerData();
            Mage::getSingleton('adminhtml/session')->getBannerData(null);
        } elseif (Mage::registry('promoblock_data')) {
            $data = Mage::registry('promoblock_data')->getData();
        }
        $this->setForm($form);
        $form->setValues($data);
        $form->setUseContainer(true);

        return parent::_prepareForm();
    }
}