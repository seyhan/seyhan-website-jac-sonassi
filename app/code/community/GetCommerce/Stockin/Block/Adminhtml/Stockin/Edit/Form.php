<?php

class GetCommerce_Stockin_Block_Adminhtml_Stockin_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    const HELPER = 'getcommerce_stockin';

    protected function _prepareForm()
    {
        $helper = Mage::helper(self::HELPER);
        $form = new Varien_Data_Form(array(
            'id'      => 'edit_form',
            'action'  => $this->getUrl('*/*/run'),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset(
            'fieldset_info', array(
            'legend' => $helper->__('Information'))
        );

        $fieldset->addField('id', 'hidden', array(
            'name'     => 'id',
            'required' => true
        ));

        $fieldset->addField('file_name', 'text', array(
            'name'     => 'file_name',
            'readonly' => true,
            'disabled' => true,
            'label'    => Mage::helper(self::HELPER)->__('File Name'),
            'title'    => Mage::helper(self::HELPER)->__('File Name'),
            'required' => true
        ));

        $data = array();

        if (Mage::getSingleton('adminhtml/session')->getData('getcommerce_stockin')) {
            $data = Mage::getSingleton('adminhtml/session')->getData('getcommerce_stockin');
        } elseif (Mage::registry('getcommerce_stockin')) {
            $data = Mage::registry('getcommerce_stockin')->getData();
        }

        $this->setForm($form);
        $form->setUseContainer(true);
        $form->setValues($data);

        return parent::_prepareForm();
    }
}