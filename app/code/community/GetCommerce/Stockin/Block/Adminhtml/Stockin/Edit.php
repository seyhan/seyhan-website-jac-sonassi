<?php

class GetCommerce_Stockin_Block_Adminhtml_Stockin_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    const HELPER = 'getcommerce_stockin';
    const REGISTRY_KEY = 'getcommerce_stockin';

    public function __construct()
    {
        parent::__construct();
        $helper = Mage::helper(self::HELPER);
        $this->_objectId = 'id';
        $this->_blockGroup = 'getcommerce_stockin';
        $this->_controller = 'adminhtml_stockin';

        $this->_addButton('runimport', array(
            'label' => Mage::helper('adminhtml')->__('Run'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_removeButton('save');
        $this->_removeButton('reset');

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper(self::HELPER)->__('Run');
    }
}