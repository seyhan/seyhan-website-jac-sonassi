<?php

class GetCommerce_Stockin_Block_Adminhtml_Stockin_New
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    const HELPER = 'getcommerce_stockin';
    const REGISTRY_KEY = 'getcommerce_stockin';

    protected $_mode = 'new';

    public function __construct()
    {
        parent::__construct();
        $helper = Mage::helper(self::HELPER);
        $this->_objectId = 'id';
        $this->_blockGroup = 'getcommerce_stockin';
        $this->_controller = 'adminhtml_stockin';

        $this->_updateButton('save', 'label', $helper->__('Save'));
        $this->_updateButton('delete', 'label', $helper->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper(self::HELPER)->__('Add New');
    }
}