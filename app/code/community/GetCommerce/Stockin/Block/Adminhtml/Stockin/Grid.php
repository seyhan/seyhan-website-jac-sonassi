<?php

class GetCommerce_Stockin_Block_Adminhtml_Stockin_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{
    const HELPER = 'getcommerce_stockin';
    protected $_helper;

    public function __construct()
    {
        $this->_helper = Mage::helper(self::HELPER);
        parent::__construct();
        $this->setId('getcommerce_stockin');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('getcommerce_stockin/data')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => $this->_helper->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'id',
        ));

        $this->addColumn('file_name', array(
            'header'    => $this->_helper->__('File Name'),
            'index'     => 'file_name',
        ));

        $this->addColumn('creation_time', array(
            'header'    => $this->_helper->__('Created At'),
            'index'     => 'creation_time',
        ));

        $this->addColumn('action',
            array(
                'header'    =>  $this->_helper->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => $this->_helper->__('View'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
            ));

        return parent::_prepareColumns();
    }


    /**
     * @param $row
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}