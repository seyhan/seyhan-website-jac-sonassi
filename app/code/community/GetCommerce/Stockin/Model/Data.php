<?php

class GetCommerce_Stockin_Model_Data extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('getcommerce_stockin/data');
    }

    public function getItemCollection()
    {
        $items = $this->getImportDataArray();
        $skus = array_keys($items);

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToFilter('sku', array('in' => $skus));

        Mage::getModel('cataloginventory/stock')->addItemsToProducts($collection);

        return $collection;
    }

    public function getImportProductCollection()
    {
        $collection = $this->getItemCollection();
        $items = $this->getImportDataArray();
        foreach($items as $itemSku => $itemQty) {
            $product = $collection->getItemByColumnValue('sku', $itemSku);
            if($product) {
                $stockItem = $product->getStockItem();
                $product->setData('update_stock_qty_by', $itemQty);
            }

        }
        return $collection;
    }

    public function updateStockItems()
    {
        $collection = $this->getItemCollection();
        $items = $this->getImportDataArray();

        foreach($items as $itemSku => $itemQty) {
            $product = $collection->getItemByColumnValue('sku', $itemSku);
            if($product) {
                $stockItem = $product->getStockItem();
                $stockItem->setStockStatusChangedAutomaticallyFlag(true);
                $stockItem->addQty($itemQty);
                if($itemQty > $stockItem->getMinQty()) {
                    $stockItem->setIsInStock(1);
                }
                $stockItem->save();
            }
        }
    }

    public function getImportDataArray()
    {
        $items = array();
        $importData = (array) json_decode($this->getImportData());
        foreach($importData as $item) {
            $items[$item->sku] = $item->update_qty;
        }
        return $items;
    }
}