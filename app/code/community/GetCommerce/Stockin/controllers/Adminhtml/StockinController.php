<?php

class GetCommerce_Stockin_Adminhtml_StockinController extends Mage_Adminhtml_Controller_Action
{
    protected $_helper;
    protected $_imageFiles = array('image', 'alternate_image');

    protected function _construct()
    {
        $this->_helper = Mage::helper('getcommerce_stockin');
    }

	protected function _initAction()
    {
		$this->loadLayout()->_setActiveMenu('cms/promoblocks');
		return $this;
	}
 
	public function indexAction()
    {
		$this->_title($this->__('Stock Updater'))
			->_title($this->__('Stock Updater'));

		$this->_initAction()
			->renderLayout();
	}

	public function newAction()
    {
        $this->_initAction();
        $this->_title($this->__('Stock Data'))
            ->_title($this->__('Stock Data'));
        $this->_title($this->__('New Stock Data'));

        $this->_setActiveMenu('cms/promoblocks');

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent(
            $this->getLayout()->createBlock('getcommerce_stockin/adminhtml_stockin_new')
        );
        $this->renderLayout();
	}
 
	public function editAction()
	{
        $this->_initAction();
        $id = $this->getRequest()->getParam('id');
        $model  = Mage::getModel('getcommerce_stockin/data')->load($id);
        Mage::register('stockin_data', $model);
        if ($model->getId() || $id == 0) {

            if (isset($data)) {
                $model->setData($data);
            }

            $this->_title($this->__('Stock Data'))
                ->_title($this->__('Stock Data'));
            if ($model->getId()){
                $this->_title($model->getTitle());
            } else {
                $this->_title($this->__('New Stock Data'));
            }

            $this->_setActiveMenu('cms/promoblocks');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('getcommerce_stockin/adminhtml_stockin_edit'));
            $this->_addContent($this->getLayout()->createBlock('getcommerce_stockin/adminhtml_stockin_edit_grid'));
            Mage::register('getcommerce_stockin', $model);
            $this->renderLayout();

        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('getcommerce_stockin')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
	}
 
	public function saveAction()
    {
        $result = $this->upload();

        $csv = new Varien_File_Csv();
        $csvData = $csv->getData($result['path'] . DS . $result['file']);
        $importData = array();
        foreach($csvData as $row) {
            $importData[] = array(
                'sku' => $row[0],
                'update_qty' => (int) $row[1]
            );
        }

        // Remove Header Row
        array_shift($importData);

        $importJson = Mage::helper('core')->jsonEncode($importData);

        $model = Mage::getModel('getcommerce_stockin/data');

        $model->setData('file_name', $result['file']);
		$model->setData('import_data', $importJson);

        try {
            $model->save();
            Mage::getSingleton('adminhtml/session')->addSuccess(('Successfully saved'));
            Mage::getSingleton('adminhtml/session')->setFormData(false);

            if ($this->getRequest()->getParam('back')) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;
            }
            $this->_redirect('*/*/');
            return;
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setFormData($data);
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('getcommerce_stockin')->__('Unable to save'));
        $this->_redirect('*/*/');
	}

    public function runAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('getcommerce_stockin/data')->load($id);
        try {
            $model->updateStockItems();
        } catch (exception $e) {
            Mage::logException($e);
        }
        $this->_redirect('*/*/');
    }
 
	public function deleteAction()
    {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('getcommerce_stockin/data');
				$model->setId($this->getRequest()->getParam('id'))->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    protected function getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

    /**
     * @return array
     */
    protected function upload()
    {
        $uploader  = Mage::getModel('core/file_uploader', 'stockin_data_file');
        $uploader->skipDbProcessing(true);
        $result = $uploader->save($this->_helper->getUploadPath());
        return $result;
    }
}