<?php

class GetCommerce_Agecheck_IndexController extends Mage_Core_Controller_Front_Action
{
    public function confirmAction()
    {
        $confirm = (int) $this->getRequest()->getPost('canview');
        if(1 == $confirm) {
             Mage::getModel('core/cookie')->set('canview_confirm', 1, 3600 * 24 * 30);
        }
    }
}
