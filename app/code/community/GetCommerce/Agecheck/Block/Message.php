<?php

class GetCommerce_Agecheck_Block_Message extends Mage_Core_Block_Template
{
    public function getFormAction()
    {
        if(1 == $this->getRequest()->isSecure()) {
            $url = $this->getUrl('canview/index/confirm', array('_secure' => true));
        } else {
            $url = $this->getUrl('canview/index/confirm');
        }
        return $url;
    }

    public function canShow()
    {
        $confirmed = Mage::getModel('core/cookie')->get('canview_confirm');
        $canShow = true;
        if (1 == $confirmed) {
            $canShow = false;
        }
        return $canShow;
    }
}