<?php
$installer = $this;

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

	$setup->addAttribute(
		'catalog_category', 'short_description', array(
			'type'                       => 'text',
			'label'                      => 'Short Description',
			'input'                      => 'textarea',
			'required'                   => false,
			'sort_order'                 => 5,
			'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
			'wysiwyg_enabled'            => true,
			'is_html_allowed_on_front'   => true,
			'group'                      => 'General Information',	
	));

	$setup->addAttribute(
		'catalog_category', 'faq', array(
			'type'                       => 'text',
			'label'                      => 'FAQ',
			'input'                      => 'textarea',
			'required'                   => false,
			'sort_order'                 => 11,
			'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
			'wysiwyg_enabled'            => true,
			'is_html_allowed_on_front'   => true,
			'group'                      => 'General Information',	
	));

	$setup->addAttribute(
		'catalog_category', 'troubleshooting', array(
			'type'                       => 'text',
			'label'                      => 'Troubleshooting',
			'input'                      => 'textarea',
			'required'                   => false,
			'sort_order'                 => 12,
			'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
			'wysiwyg_enabled'            => true,
			'is_html_allowed_on_front'   => true,
			'group'                      => 'General Information',	
	));
	
	$setup->addAttribute(
		'catalog_category', 'videos', array(
			'type'                       => 'varchar',
			'label'                      => 'Videos',
			'input'                      => 'text',
			'required'                   => false,
			'sort_order'                 => 13,
			'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
			'group'                      => 'General Information',
	));
	
$installer->endSetup();
	 