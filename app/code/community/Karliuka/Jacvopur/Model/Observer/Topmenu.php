<?php

class Karliuka_Jacvopur_Model_Observer_Topmenu extends Varien_Event_Observer
{
	/**
     * remove Categories Children.
	 * @param object $observer.	 
     * @return this
    */	
	public function removeChild($observer)
    {	
		$menu = $observer->getMenu();
		if ($menu instanceof Varien_Data_Tree_Node){
			$items = $menu->getChildren();
			foreach ($items as $item){
				if($item->hasChildren()){
					$children = $item->getChildren();
					foreach ($children as $child){
						$item->removeChild($child);
					}
				}
			}
		}
		return $this;
    }
}