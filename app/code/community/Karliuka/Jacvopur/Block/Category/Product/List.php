<?php
class Karliuka_Jacvopur_Block_Category_Product_List extends Mage_Catalog_Block_Product_List
{
    /**
     * Retrieve products collection by category ID
     * @return Mage_Catalog_Model_Product_Collection
    */
    public function getCollection($ids)
    {
        $ids = (is_array($ids)) ? $ids : array($ids);
		$collection = Mage::getModel('catalog/product')
			->getCollection()
			->addStoreFilter()
			->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
			->addAttributeToSelect('*')
			->addAttributeToFilter('category_id', array('in' => $ids));
		$collection->getSelect()->order(new Zend_Db_Expr('RAND()'));
		return $collection;
    }
}