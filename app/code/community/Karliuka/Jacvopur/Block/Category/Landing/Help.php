<?php
class Karliuka_Jacvopur_Block_Category_Landing_Help extends Mage_Core_Block_Template
{
    /**
     * category Instance
     * @var Mage_Catalog_Model_Category
    */
    protected $_categoryInstance;
	
    /**
     * Retrieve child categories of current category
     * @return Varien_Data_Tree_Node_Collection
    */
    public function getCurrentChildCategories()
    {
        $category = $this->getCurrentCategory();
        return $this->getChildrenCategories($category);
    }
	
    /**
     * Return child categories
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Model_Resource_Category_Collection
    */
    public function getChildrenCategories($category)
    {
        $collection = $category->getCollection();
        $collection->addAttributeToSelect('url_key')
            ->addAttributeToSelect('name')
			->addAttributeToSelect('image')
            ->addAttributeToSelect('all_children')
            ->addAttributeToSelect('is_anchor')
            ->addAttributeToFilter('is_active', 1)
            ->addIdFilter($category->getChildren())
            ->setOrder('position', Varien_Db_Select::SQL_ASC)
            ->joinUrlRewrite()
            ->load();
        return $collection;
    }
	
    /**
     * Get Category model
     * @return Mage_Catalog_Model_Category
    */	
    protected function _getCategoryInstance()
    {
        if (is_null($this->_categoryInstance)) {
            $this->_categoryInstance = Mage::getModel('catalog/category');
        }
        return $this->_categoryInstance;
    }
	
    /**
     * Get category
     * @return Mage_Catalog_Model_Category
    */
    public function getCurrentCategory()
    {
        $layer = Mage::getSingleton('catalog/layer');
		if ($layer && $layer->getCurrentCategory()) return $layer->getCurrentCategory();
		return Mage::getModel('catalog/category')->load(2);
    }
	
    /**
     * Get url for category data
     * @param Mage_Catalog_Model_Category $category
     * @return string
    */
    public function getCategoryUrl($category)
    {
        if ($category instanceof Mage_Catalog_Model_Category) $url = $category->getUrl();
        else {
            $url = $this->_getCategoryInstance()
                ->setData($category->getData())
                ->getUrl();
        }
        return $url;
    }	
}