<?php

class Ometria_Core_Model_Observer_Order {

    /**
     * Sales Order After Save
     *
     * @param Varien_Event_Observer $observer
     * @return Ometria_Core_Model_Observer_Order
     */
    public function salesOrderSaveAfter(Varien_Event_Observer $observer) {
        Varien_Profiler::start("Ometria::" . __METHOD__);

        $order = $observer->getEvent()->getOrder();
        $this->updateOrders($order->getIncrementId());

        Varien_Profiler::stop("Ometria::" . __METHOD__);

        return $this;
    }

    /**
     * Pass order ids to Ometria API model
     *
     * @param $ids
     * @return bool
     */
    protected function updateOrders($ids) {

        /** @var Ometria_Core_Helper_Config $ometriaConfigHelper */
        $ometriaConfigHelper = Mage::helper('ometria/config');

        // If debug mode, log the product id
        if($ometriaConfigHelper->isDebugMode()) {
            if(is_array($ids)) {
                $ometriaConfigHelper->log("Updated order IDs " . implode(',', $ids));
            } else {
                $ometriaConfigHelper->log("Updated order ID " . $ids);
            }
        }

        /** @var Ometria_Core_Model_Api $ometriaAPIModel */
        $ometriaAPIModel = Mage::getModel('ometria/api');

        // Notify Ometria of Product save action
        return $ometriaAPIModel->notifyOrderUpdates($ids);
    }
}