<?php

/**
 * Class Ometria_Core_Model_Api
 */
class Ometria_Core_Model_Api extends Mage_Core_Model_Abstract {

    const API_HOST = 'trk.ometria.com';
    const API_SOCKET_SCHEMA = 'ssl://';
    const API_PATH = '/ping.php';
    const API_SOCKET_TIMEOUT = 2;

    /**
     * Notify Ometria of product updates
     *
     * @param $ids
     * @return bool
     */
    public function notifyProductUpdates($ids) {
        /** @var Ometria_Core_Helper_Config $ometriaConfigHelper */
        $ometriaConfigHelper = Mage::helper('ometria/config');

        if (!$ometriaConfigHelper->isConfigured()) {
            //$ometriaConfigHelper->log("Ping failed: Ometria extension not enabled and/or configured with site identifier.");
            return false;
        }

        return $this->ping(
            array(
                'account' => $ometriaConfigHelper->getAPIKey(),
                'type'    => 'product',
                'id'      => $ids
            )
        );
    }

    /**
     * Notify Ometria of order updates
     *
     * @param $ids
     * @return bool
     */
    public function notifyOrderUpdates($ids) {
        /** @var Ometria_Core_Helper_Config $ometriaConfigHelper */
        $ometriaConfigHelper = Mage::helper('ometria/config');

        if (!$ometriaConfigHelper->isConfigured()) {
            //$ometriaConfigHelper->log("Ping failed: Ometria extension not enabled and/or configured with site identifier.");
            return false;
        }

        return $this->ping(
            array(
                'account' => $ometriaConfigHelper->getAPIKey(),
                'type'    => 'transaction',
                'id'      => $ids
            )
        );
    }


    /**
     * Helper function to ping ometria.  Manually doing an fsockopen
     * so that we don't have to wait for a response. Unless debugging
     * when we do wait and log the content body.
     *
     * @param array $parameters
     *
     * @return bool
     */
    protected function ping($parameters = array()) {
        /** @var Ometria_Core_Helper_Config $ometriaConfigHelper */
        $ometriaConfigHelper = Mage::helper('ometria/config');

        if (!$ometriaConfigHelper->isPingEnabled())
        {
            return true;
        }

        $content = http_build_query($parameters);
        $path = self::API_PATH;


        try {

            $fp = fsockopen(self::API_SOCKET_SCHEMA . self::API_HOST, 443, $errorNum, $errorStr, self::API_SOCKET_TIMEOUT);

            if($fp !== false) {

                $out  = "POST $path HTTP/1.1\r\n";
                $out .= "Host: " . self::API_HOST. "\r\n";
                $out .= "Content-type: application/x-www-form-urlencoded\r\n";
                $out .= "Content-Length: " . strlen($content) . "\r\n";
                $out .= "Connection: Close\r\n\r\n";
                $out .= $content;

                fwrite($fp, $out);

                // If debug mode, wait for response and log
                if($ometriaConfigHelper->isDebugMode()) {

                    $responseHeader = "";
                    do {
                        $responseHeader .= fgets($fp, 1024);
                    } while(strpos($responseHeader, "\r\n\r\n") === false);

                    $response = "";
                    while (!feof($fp)) {
                        $response .= fgets($fp, 1024);
                    }

                    $ometriaConfigHelper->log($response);
                }

                fclose($fp);
            } else {
                $ometriaConfigHelper->log("Ping failed: Error $errorNum - $errorStr", Zend_Log::ERR);
                return false;
            }
        } catch (Exception $e) {
            $ometriaConfigHelper->log($e->getMessage());
            return false;
        }

        return true;
    }

}