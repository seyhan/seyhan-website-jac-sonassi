<?php

class Ometria_Core_Model_Container_Head extends Enterprise_PageCache_Model_Container_Advanced_Abstract {

    const CACHE_TAG_PREFIX = 'ometria_';

    /**
     * Get identifier from cookies
     *
     * @deprecated since 1.12.1.0
     * @return string
     */
    protected function _getIdentifier()
    {
        return $this->_getCookieValue(Enterprise_PageCache_Model_Cookie::COOKIE_CART, '')
        . $this->_getCookieValue(Enterprise_PageCache_Model_Cookie::COOKIE_CUSTOMER, '');
    }

    /**
     * Get cache identifier
     *
     * @return string
     */
    protected function _getCacheId()
    {
        $cookieCart = $this->_getCookieValue(Enterprise_PageCache_Model_Cookie::COOKIE_CART, '');
        $cookieCustomer = $this->_getCookieValue(Enterprise_PageCache_Model_Cookie::COOKIE_CUSTOMER, '');
        return self::CACHE_TAG_PREFIX
        . md5(
            $this->_getRequestId()
            . $this->_placeholder->getAttribute("page_number")
            . $this->_placeholder->getAttribute("items_per_page")
            . $this->_placeholder->getAttribute("basket_id")
            . $this->_placeholder->getAttribute("basket_updated_at")
            . $this->_placeholder->getAttribute("order_id")
        )
        . ($cookieCart ? "_" . $cookieCart : '')
        . ($cookieCustomer ? "_" . $cookieCustomer : '');
    }

    /**
     * Render block content
     *
     * @return string
     */
    protected function _renderBlock()
    {
        $blockClass = $this->_placeholder->getAttribute('block');
        $template = $this->_placeholder->getAttribute('template');

        $block = new $blockClass;
        $block
            ->setTemplate($template)
            ->setProductId($this->_placeholder->getAttribute("product_id"))
            ->setPaginationInfo(array(
                "page"           => $this->_placeholder->getAttribute("page_number"),
                "items_per_page" => $this->_placeholder->getAttribute("items_per_page")
            ));

        return $block->toHtml();
    }

    /**
     * Get container individual additional cache id
     *
     * @return string
     */
    protected function _getAdditionalCacheId()
    {
        return md5($this->_placeholder->getName() . '_' . $this->_placeholder->getAttribute('cache_id'));
    }


    /**
     * Get current request id
     *
     * @return string|null
     */
    protected function _getRequestId()
    {
        return !$this->_processor ? null : $this->_processor->getRequestId();
    }
}
