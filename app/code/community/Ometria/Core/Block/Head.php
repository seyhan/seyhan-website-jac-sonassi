<?php

class Ometria_Core_Block_Head extends Mage_Core_Block_Template {

    const UV_VERSION             = "1.1.1";
    const PAGE_TYPE_BASKET       = 'basket';
    const PAGE_TYPE_CHECKOUT     = 'checkout';
    const PAGE_TYPE_CMS          = 'landing';
    const PAGE_TYPE_CATEGORY     = 'listing';
    const PAGE_TYPE_CONFIRMATION = 'confirmation';
    const PAGE_TYPE_HOMEPAGE     = 'homepage';
    const PAGE_TYPE_PRODUCT      = 'product';
    const PAGE_TYPE_SEARCH       = 'search';
    const OM_CHECKOUT_STAGE      = 'om:checkout_stage';
    const OM_PAGINATION          = 'om:pagination';
    const OM_QUERY               = 'om:query';
    const OM_SITE                = 'om:site';

    public function getCacheKeyInfo() {
        $info = parent::getCacheKeyInfo();

        if (Mage::registry("current_product")) {
            $info["product_id"] = Mage::registry("current_product")->getId();
        }

        if ($pagination_info = $this->getPaginationInfo()) {
            $info["page_number"] = $pagination_info["page"];
            $info["items_per_page"] = $pagination_info["items_per_page"];
        }

        $quote = $this->_getCheckoutSession()->getQuote();
        if ($basket_id = $quote->getId()) {
            $info["basket_id"] = $basket_id;
            $info["basket_updated_at"] = str_replace(" ", "", $quote->getUpdatedAt());
        }

        if ($order_id = $this->_getCheckoutSession()->getLastOrderId()) {
            $info["order_id"] = $order_id;
        }

        return $info;
    }


    /**
     * Determine what page the customer is on.
     * @return string
     */
    public function getPage() {
        $category = 'null';
        $page = array();

        if ($this->_isHomepage()) {
            $category = self::PAGE_TYPE_HOMEPAGE;

        } elseif ($this->_isCMSPage()) {
            $category = self::PAGE_TYPE_CMS;

        } elseif ($this->_isCategory()) {
            $category = self::PAGE_TYPE_CATEGORY;

            if($pagination = $this->getPaginationInfo())
                $page[self::OM_PAGINATION] = $pagination;

        } elseif ($this->_isSearch()) {
            $category = self::PAGE_TYPE_SEARCH;

            if($pagination = $this->getPaginationInfo())
                $page[self::OM_PAGINATION] = $pagination;

            if($query = $this->_getSearchQuery())
                $page[self::OM_QUERY] = $query;

        } elseif ($this->_isProduct()) {
            $category = self::PAGE_TYPE_PRODUCT;

        } elseif ($this->_isBasket()) {
            $category = self::PAGE_TYPE_BASKET;

        } elseif ($this->_isCheckout()) {
            $category = self::PAGE_TYPE_CHECKOUT;

            if ($step = $this->_getCheckoutStep())
                $page[self::OM_CHECKOUT_STAGE] = $step;

        } elseif ($this->_isOrderConfirmation()) {
            $category = self::PAGE_TYPE_CONFIRMATION;
        }

        $page[self::OM_SITE] = $this->_getStore();
        $page['category'] = $category;

        return $page;
    }

    /**
     * Provide customer information
     * @return array
     */
    public function getUser() {
        $user = array(
            'language' => Mage::getStoreConfig('general/locale/code', Mage::app()->getStore()->getId()),
            'returning' => false
        );

        if ($this->_getCustomer()->getEmail()) {
            $customer = $this->_getCustomer();
            $user['name'] = $customer->getFirstname() . " " . $customer->getLastname();
            $user['user_id'] = $customer->getId();
            $user['email'] = $customer->getEmail();
            $user['returning'] = true;
        }

        return $user;
    }

    /**
     * Provide category listing information
     * Also used for search result pages
     * - 21 Nov 2013: disabled this for speed
     * - 27 Mar 2014: re-enabled. Moving listing call to footer addresses speed issues.
     * @return array
     */
    public function getListing() {
        $listing = array();
        /*if($productListBlock = $this->_getProductListBlock()) {
            $productCollection = $productListBlock->getLoadedProductCollection();

            foreach ($productCollection as $product) {
                $listing[] = $product->getId();
            }

            $productCollection->clear();
        }*/
        return $listing;
    }

    /**
     * Provide basket information
     * @return array
     */
    public function getBasket() {
        $quote = $this->_getCheckoutSession()->getQuote();

        if ($basketId = $quote->getId()) {
            $shippingAddress = $quote->getShippingAddress();
            $items = $quote->getItemsCollection();

            return array(
                'id'                   => $basketId,
                'currency'             => $quote->getQuoteCurrencyCode(),
                'subtotal'             => (float) $quote->getSubtotal(),
                'subtotal_include_tax' => $this->isCartSubtotalTaxInclusive(),
                'tax'                  => (float) $shippingAddress->getTaxAmount(),
                'shipping_cost'        => (float) $shippingAddress->getShippingAmount(),
                'shipping_method'      => $shippingAddress->getShippingMethod(),
                'voucher'              => $shippingAddress->getDiscountDescription(),
                'voucher_discount'     => (float) abs($shippingAddress->getDiscountAmount()),
                'total'                => (float) $quote->getGrandTotal(),
                'line_items'           => $this->_getLineItems($items, 'basket')
            );
        }

        return false;
    }

    /**
     * Provide transaction information
     * @return array
     */
    public function getTransaction() {
        if (!$this->_isOrderConfirmation())
            return false;

        if ($orderId = $this->_getCheckoutSession()->getLastOrderId()) {
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load($orderId);

            return array(
                'order_id'              => $order->getIncrementId(),
                'currency'              => $this->_getCurrency(),
                'subtotal'              => (float) $order->getSubtotal(),
                'subtotal_include_tax'  => $this->isOrderSubtotalTaxInclusive($order),
                'payment_type'          => $order->getPayment()->getMethodInstance()->getTitle(),
                'total'                 => (float) $order->getGrandTotal(),
                'tax'                   => (float) $order->getTaxAmount(),
                'shipping_cost'         => (float) $order->getShippingInclTax(),
                'shipping_method'       => $order->getShippingDescription(),
                'voucher'               => $order->getDiscountDescription(),
                'voucher_discount'      => (float) abs($order->getDiscountAmount()),
                'line_items'            => $this->_getLineItems($order->getAllItems(), 'transaction')
            );
        }

        return false;
    }

    /**
     * Get Universal Variable Specification version
     * @return string
     */
    public function getVersion() {
        return self::UV_VERSION;
    }

    /**
     * Get module version
     * @return string
     */
    public function getMagentoVersion() {
        return Mage::getVersion();
    }

    /**
     * Get product information, if available
     * @return array
     */
    public function getProduct() {
        $product = null;

        if ($id = $this->getProductId()) {
            $product = Mage::getModel("catalog/product")->load($id);
        } else {
            $product = $this->_getCurrentProduct();
        }

        if ($product) {
            return $this->_getProductInfo($product);
        }

        return false;
    }

    /**
     * Get category page separated by ::
     * @return string
     */
    public function getCategoryPath() {
        /** @var Mage_Catalog_Model_Category category */
        if ($category = Mage::registry("current_category")) {
            return Mage::helper('ometria/category')->getCategoryPath($category);
        }

        return false;
    }

    /**
     * Get category and search page info.
     * @return array
     */
    public function getPaginationInfo() {
        if (!$this->getData("pagination_info")) {
            if ($this->_isCategory() || $this->_isSearch()) {
                /** @var Mage_Catalog_Block_Product_List_Toolbar $toolbar */
                if ($toolbar = $this->_getToolbarBlock()) {
                    $this->setData("pagination_info", array(
                        'page'           => $toolbar->getCurrentPage(),
                        'items_per_page' => $toolbar->getLimit()
                    ));
                }
            }
        }

        return $this->getData("pagination_info");
    }


    /**
     * Is the cart subtotal inclusive of tax
     * @return bool
     */
    protected function isCartSubtotalTaxInclusive() {
        return  (Mage::getStoreConfig('tax/cart_display/subtotal') > 1);
    }

    /**
     * Is the order subtotal inclusive of tax
     * @param $order Mage_Sales_Model_Order
     * @return bool
     */
    protected function isOrderSubtotalTaxInclusive($order) {
        return ($order->getTaxAmount() == 0 || $order->getSubtotal() == $order->getSubtotalInclTax());
    }

    /**
     * Get Controller name
     * @return string
     */
    protected function _getControllerName() {
        return $this->getRequest()->getRequestedControllerName();
    }

    /**
     * Get Action name
     * @return string
     */
    protected function _getActionName() {
        return $this->getRequest()->getRequestedActionName();
    }

    /**
     * Get Route name
     * @return string
     */
    protected function _getRouteName() {
        return $this->getRequest()->getRequestedRouteName();
    }

    /**
     * Check if home page
     * @return bool
     */
    protected function _isHomepage() {
        return $this->getUrl('') == $this->getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true));
    }

    /**
     * Check if cms page
     * @return bool
     */
    protected function _isCMSPage() {
        return $this->_getRouteName() == 'cms';
    }

    /**
     * Check if category page
     * @return bool
     */
    protected function _isCategory() {
        return $this->_getRouteName()       == 'catalog'
            && $this->_getControllerName()  == 'category';
    }

    /**
     * Check if search page
     * @return bool
     */
    protected function _isSearch() {
        return $this->_getRouteName() == 'catalogsearch';
    }

    /**
     * Check if product page
     * @return bool
     */
    protected function _isProduct() {
        return $this->_getRouteName()      == 'catalog'
            && $this->_getControllerName() == 'product';
    }

    /**
     * Check if basket
     * @return bool
     */
    protected function _isBasket() {
        return $this->_getRouteName()           == 'checkout'
                && $this->_getControllerName()  == 'cart'
                && $this->_getActionName()      == 'index';
    }

    /**
     * Check if checkout
     * @return bool
     */
    protected function _isCheckout() {
        return strpos($this->_getRouteName(), 'checkout') !== false
                && $this->_getActionName()  != 'success';
    }

    /**
     * Check if success page
     * @return bool
     */
    protected function _isOrderConfirmation() {
        return strpos($this->_getRouteName(), 'checkout') !== false
                && $this->_getActionName() == 'success';
    }

    /**
     * Get Magento customer
     * @return Mage_Customer_Model_Customer
     */
    protected function _getCustomer() {
        return Mage::helper('customer')->getCustomer();
    }

    protected function _getCheckoutSession() {
        if ($this->_isBasket())
            return Mage::getSingleton('checkout/cart');

        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current category
     * @return Mage_Catalog_Model_Category
     */
    protected function _getCurrentCategory() {
        return Mage::registry("current_category");
    }

    /**
     * Get current product
     * @return Mage_Catalog_Model_Product
     */
    protected function _getCurrentProduct() {
        return Mage::registry("current_product");
    }

    /**
     * Given cart or order items return item array
     * @param $items
     * @param $pageType
     * @return array
     */
    protected function _getLineItems($items, $pageType) {
        $lineItems = array();

        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach($items as $item) {

            /** @var Mage_Catalog_Model_Product $product */
            $product = $item->getProduct();

            if(!$product || $product->getId() == "-1") {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());
            }

            if ($product && $product->isVisibleInSiteVisibility()) {
                if ($pageType == self::PAGE_TYPE_BASKET) {
                    $qty = round($item->getQty(), 0);
                } else {
                    $qty = round($item->getQtyOrdered(), 0);
                }

                $lineItem = array(
                    'product'        => $this->_getProductInfo($product),
                    'subtotal'       => (float) $item->getRowTotalInclTax(),
                    'total_discount' => (float) $item->getDiscountAmount(),
                    'quantity'       => $qty
                );

                $lineItems[] = $lineItem;
            }
        }
        return $lineItems;
    }


    /**
     * Get limited product info from product
     * Used in listing, baskets, transactions
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    protected function _getProductInfo($product) {
        if($product instanceof Mage_Catalog_Model_Product) {
            return array(
                'id'                              => $product->getId(),
                'sku_code'                        => $product->getSku(),
                'name'                            => $product->getName(),
                'url'                             => $product->getProductUrl()
            );
        }

        return false;
    }

    /**
     * Get frontend visible attribute data
     * @param $product
     * @return array
     */
    protected function getFrontendAttributes($product) {
        $data = array();

        $attributes = $product->getAttributes();

        foreach ($attributes as $attribute) {
            if ($attribute->getIsVisibleOnFront()) {
                $value = $attribute->getFrontend()->getValue($product);

                if (!$product->hasData($attribute->getAttributeCode())) {
                    continue;
                } elseif ((string)$value == '') {
                    $value = Mage::helper('catalog')->__('No');
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = Mage::app()->getStore()->convertPrice($value, true);
                }

                // Do not include really long attributes. They are rarely used.
                if (is_string($value) && strlen($value)>32) continue;

                if (is_string($value) && strlen($value)) {
                    $data[] = array($attribute->getStoreLabel(), $value);
                }
            }
        }

        return $data;
    }

    /**
     * Get currency code
     * @return string
     */
    protected function _getCurrency() {
        return Mage::app()->getStore()->getCurrentCurrencyCode();
    }

    /**
     * Get checkout step
     * @return string
     */
    protected function _getCheckoutStep() {
        if(!$this->_isCheckout())
            return false;

        if($step = Mage::app()->getRequest()->getParam('step'))
            return $step;

        return false;
    }

    /**
     * Get product list toolbar block
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    protected function _getToolbarBlock() {
        if ($this->getLayout()) {
            return $this->getLayout()->getBlock('product_list_toolbar');
        }
    }

    /**
     * Get product list block
     * @return Mage_Catalog_Block_Product_List
     */
    protected function _getProductListBlock() {
        $blockName = '';
        if ($this->_isCategory()) {
            $blockName = 'product_list';
        } elseif($this->_isSearch()) {
            $blockName = 'search_result_list';
        }

        if ($this->getLayout()) {
            return $this->getLayout()->getBlock($blockName);
        }
    }

    /**
     * Get Store id
     * @return string|int
     */
    protected function _getStore() {
        $stores = Mage::app()->getStores();

        return Mage::app()->getStore()->getStoreId();
    }

    /**
     * Get search query text
     * @return string
     */
    protected function _getSearchQuery() {
        if(!$this->_isSearch())
            return false;

        return Mage::helper('catalogsearch')->getQueryText();
    }
}
