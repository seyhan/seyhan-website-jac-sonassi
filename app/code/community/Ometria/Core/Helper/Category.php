<?php

class Ometria_Core_Helper_Category extends Mage_Core_Helper_Abstract {

    /**
     * Get Ometria formatted category path
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getCategoryPath($category) {
        $path = array();

        if ($category instanceof Mage_Catalog_Model_Category) {
            
            // Disabled for speed
            // Instead we work out path based on URL PATH
            /*$categories = Mage::getModel('catalog/category')->getCollection()
                ->addFieldToFilter('entity_id', array('in' => $category->getPathIds()))
                ->setStoreId(Mage::app()->getStore()->getId())
                ->addFieldToFilter('level', array('gt' => 1))
                ->addFieldToFilter('is_active', array('eq'=>'1'))
                ->addAttributeToSelect('name')
                ->addAttributeToSort('level');

            $helper = Mage::helper('ometria');
            
            foreach ($categories as $category) {
                $path[] = $helper->_normaliseString($category->getName());
            }*/

            $helper = Mage::helper('ometria');

            $url_path = $category->url_path;
            $url_path = str_replace('.html', '', $url_path);
            $parts = explode("/", $url_path);
            foreach ($parts as $part) {
                $path[] = $helper->_normaliseString($part);
            }
        }

        return implode("::",$path);
    }
}