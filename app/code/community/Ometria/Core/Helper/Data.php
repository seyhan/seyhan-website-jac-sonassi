<?php

class Ometria_Core_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * Get extension version
     * @return string
     */
    public function getExtensionVersion() {
        return Mage::getConfig()->getModuleConfig('Ometria_Core')->version;
    }

    /**
     * Lower case strings and remove characters we don't like
     * @param $string
     * @return mixed
     */
    public function _normaliseString($string) {
        $string = (function_exists('mb_strtolower')) ? mb_strtolower($string, mb_detect_encoding($string)) : strtolower($string);
        $string = trim($string);
        return preg_replace('/[^a-z0-9\-\_]+/', '', $string);
    }

}