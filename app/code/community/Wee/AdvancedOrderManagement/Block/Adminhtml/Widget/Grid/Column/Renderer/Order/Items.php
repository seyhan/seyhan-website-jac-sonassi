<?php

class Wee_AdvancedOrderManagement_Block_Adminhtml_Widget_Grid_Column_Renderer_Order_items
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract 
{
    public function render(Varien_Object $row)
    {
        $block = $this->getLayout()->createBlock('wee_advanced_order_management_adminhtml/sales_order_grid_order_items');
        $block->setItems($row->getItems());
        return $block->toHtml();
    }
}