<?php

class Wee_AdvancedOrderManagement_Block_Adminhtml_Sales_Order_Grid_Order_Items extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('wee_advanced_order_management/sales/order/grid/order/items.phtml');
    }
    
    public function getThumbnailHtml(Mage_Sales_Model_Order_Item $item, $width = 65)
    {
        $thumbnail = $item->getData('thumbnail');
        $imageHtml = '<img src="'. Mage::helper('wee_advanced_order_management/image')->init(Mage::getModel('catalog/product'), 'thumbnail', $thumbnail)->resize($width). '" />';
        return $imageHtml;
    }
}