<?php

class Wee_AdvancedOrderManagement_Model_Resource_Order_Address_Collection extends Mage_Sales_Model_Resource_Order_Address_Collection
{
    public function filterbyOrders(Mage_Sales_Model_Resource_Order_Collection $orders)
    {
        $orderIds = array();
        foreach ($orders as $order) {
            $orderIds[] = $order->getId();
        }
        $this->addFieldToFilter('parent_id', array('in' => $orderIds));
        return $this;
    }
}