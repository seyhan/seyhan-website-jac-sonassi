<?php

class Wee_AdvancedOrderManagement_Helper_Image extends Mage_Catalog_Helper_Image
{
	public function init(Mage_Catalog_Model_Product $product, $attributeName, $imageFile=null)
    {
        $this->_reset();
        $this->_setModel(Mage::getModel('catalog/product_image'));
        $this->_getModel()->setDestinationSubdir($attributeName);

        $this->setImageFile($imageFile);
        return $this;
    }
    
    public function __toString()
    {
        try {
            if ($this->getImageFile()) {
                $this->_getModel()->setBaseFile( $this->getImageFile() );
            } else {
            	throw new Exception('Image does not exists');
            }

            if( $this->_getModel()->isCached() ) {
                return $this->_getModel()->getUrl();
            } else {
                if( $this->_scheduleRotate ) {
                    $this->_getModel()->rotate( $this->getAngle() );
                }

                if ($this->_scheduleResize) {
                    $this->_getModel()->resize();
                }

                if( $this->getWatermark() ) {
                    $this->_getModel()->setWatermark($this->getWatermark());
                }

                $url = $this->_getModel()->saveFile()->getUrl();
            }
        } catch( Exception $e ) {
        	$design = clone(Mage::getDesign());
        	$design->setArea('frontend');
            $url = $design->getSkinUrl($this->getPlaceholder());
        }
        return $url;
    }
}
