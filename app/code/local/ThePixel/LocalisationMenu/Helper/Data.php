<?php
class ThePixel_LocalisationMenu_Helper_Data extends Mage_Core_Helper_Abstract
{	
	public function getCurrentStoreCode()
    {
    	return Mage::app()->getStore()->getCode();
    }
    
	public function getCurrentCurrencyCode()
    {
    	return Mage::app()->getStore()->getCurrentCurrencyCode();
    }
    
    public function getCurrentCurrencySymbol()
    {
    	return Mage::app()->getLocale()->currency( $this->getCurrentCurrencyCode() )->getSymbol();
    }
}
?>