<?php
/**
 * Magento Mad Capsule Media Royal Mail Despatch Express  Extension
 * http://www.madcapsule.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @copyright  Copyright (c) 2009 Mad Capsule Media (http://www.madcapsule.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     James Mikkelson <james@madcapsule.co.uk>
*/
class MadCapsule_Rmde_Block_Adminhtml_Rmde extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_rmde';
        $this->_blockGroup = 'rmde';
        $this->_headerText = Mage::helper('rmde')->__('Consignment Recovery');
		$this->_addButton('purge', array(
            'label'     => Mage::helper('rmde')->__('Disengage Current Label'),
			'onclick'   => "setLocation('".$this->getUrl('*/*/disengage')."')",
        ));
        parent::__construct();
		$this->removeButton('add');
    }
}
