<?php
/**
 * Magento Mad Capsule Media Royal Mail Despatch Express  Extension
 * http://www.madcapsule.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @copyright  Copyright (c) 2009 Mad Capsule Media (http://www.madcapsule.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     James Mikkelson <james@madcapsule.co.uk>
*/
class MadCapsule_Rmde_Block_Adminhtml_Rmde_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('rmdeGrid');
        $this->setDefaultSort('rmde_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('rmde/consignment')->getCollection();
		$collection->addFieldToFilter('transfer_id', array('gt' => '1'));
        $this->setCollection($collection);		
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('rmde_id', array(
            'header'    => Mage::helper('rmde')->__('ID'),
            'align'     =>'right',
            'width'     => '20px',
            'index'     => 'transfer_id',
        ));

        $this->addColumn('service', array(
            'header'    => Mage::helper('rmde')->__('Service'),
            'align'     =>'right',
            'width'     => '20px',
            'index'     => 'service',
        ));
		
		
        $this->addColumn('format', array(
            'header'    => Mage::helper('rmde')->__('Format'),
            'align'     =>'right',
            'width'     => '10px',
            'index'     => 'format',
        ));

        $this->addColumn('shipment', array(
            'header'    => Mage::helper('rmde')->__('Shipment'),
            'align'     =>'right',
            'width'     => '20px',
            'index'     => 'shipment_id',
        ));
		
        $this->addColumn('order', array(
            'header'    => Mage::helper('rmde')->__('Order'),
            'align'     =>'right',
            'width'     => '20px',
            'index'     => 'order_id',
        ));

		$this->addColumn('weight', array(
            'header'    => Mage::helper('rmde')->__('Weight'),
            'align'     =>'right',
            'width'     => '20px',
            'index'     => 'weight',
        ));
		
        $this->addColumn('status', array(
 
            'header'    => Mage::helper('rmde')->__('Status'),
            'align'     => 'left',
            'width'     => '150px',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => array(
                1 => 'Pending Transfer',
                2 => 'Transfered Pending Despatch',
                3 => 'Label Printed',
                4 => 'Label Failed'
            ),
        ));
		
        $this->addColumn('code_msg', array(
 
            'header'    => Mage::helper('rmde')->__('Despatch Message'),
            'align'     => 'left',
            'width'     => '300px',
            'index'     => 'code_msg',
        ));
        
        $this->addColumn('email', array(
 
            'header'    => Mage::helper('rmde')->__('Tracking Email Address'),
            'align'     => 'left',
            'width'     => '300px',
            'index'     => 'email',
        	'default'	=> 'Tracking email not sent',
        ));
 
        $this->addColumn('sent_time', array(
            'header'    => Mage::helper('rmde')->__('Queued '),
            'align'     => 'left',
            'width'     => '100px',
            'type'      => 'date',
            'default'   => '--',
            'index'     => 'sent',
        ));
 
        $this->addColumn('returned_time', array(
            'header'    => Mage::helper('rmde')->__('Returned'),
            'align'     => 'left',
            'width'     => '100px',
            'type'      => 'date',
            'default'   => 'Trackin not yet returned',
            'index'     => 'returned',
        ));  

        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('rmde')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('rmde')->__('Re-despatch'),
                        'url'       => array('base'=> '*/*/resubmit'),
                        'field'     => 'id'
                    ),
                    array(
                        'caption'   => Mage::helper('rmde')->__('Cancel'),
                        'url'       => array('base'=> '*/*/delete'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));

    
 
 
        return parent::_prepareColumns();
    }
 
    public function getRowUrl($row)
    {
        //return $this->getUrl('*/*/resubmit', array('id' => $row->getId()));
    }
 
 
}
