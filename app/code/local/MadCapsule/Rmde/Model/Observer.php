<?php
/**
 * Magento Mad Capsule Media RMDMO Extension
 * http://www.madcapsule.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @copyright  Copyright (c) 2009 Mad Capsule Media (http://www.madcapsule.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     James Mikkelson <james@madcapsule.co.uk>
 * @filever 0.0.1
 *
*/
class MadCapsule_Rmde_Model_Observer
{

	public function forceConsignment(Varien_Event_Observer $observer)
		{
			$mode=1;$mode=Mage::getStoreConfig('rmde/misc/mode');
			
			if($mode>1)
				{
				
					if(Mage::getStoreConfig('rmde/misc/format')=='')
					{
						$format =  'P';
					}else{
						$format =  strtoupper(Mage::getStoreConfig('rmde/misc/format'));				
					}
	
					$shipment_id = $observer->getEvent()->getShipment()->getId();
					$shipment_increment = $observer->getEvent()->getShipment()->getIncrementId();
					$_order = Mage::getModel('sales/order')->load($observer->getEvent()->getShipment()->getOrderId());
					$order_inc_id = $_order->getIncrementId();				
					$shipment = $observer->getEvent()->getShipment()->getIncrementId();
    				$consignment = Mage::getModel('rmde/consignment');
					$consignment->setShipmentId($shipment_increment);
					$consignment->setStatus(1);
					$consignment->setOrderId($order_inc_id);
					
					if($mode==3 && $_order->getShippingMethod()){
						$code_values = $this->mappedService($_order->getShippingMethod());
						$service_code = $code_values['s'];$format = $code_values['f'];
					}else{
						$service_code = $this->serviceCode();
					}					
					
					$consignment->setFormat($format);
					$consignment->setService($service_code);
					$consignment->setWeight(Mage::helper('rmde')->getWeight($shipment));
					
					if(!$service_code || $service_code == ''){
						$this->activity($shipment_increment.' is not intended for Royal Mail'); 
					}else{
						try {
							$this->activity($shipment_increment.' has been sent to Royal Mail'); 
							$consignment->save();
						}
						catch (Exception $e) {
							throw new Exception($this->__('Failed to add shipment to Royal Mail queue.'));
						}
					}					
				}

		}
		
	private function mappedService($shipping_method) 
	{
			$mapped_values = Mage::getStoreConfig('rmde/mapping/map_a');
			$exploded_values = explode('#', $mapped_values);
			$codes = array();
			foreach($exploded_values as $service){
				if($service){
					$indi_map = explode(',', $service);
					$service_id = substr(end($indi_map), 0, -1);
					$format = substr(end($indi_map), -1, 1);
					foreach($indi_map as $mapped_value){
						if($service_id!=$mapped_value && $mapped_value==$shipping_method){
							$codes['s'] = trim($service_id);
							$codes['f'] = trim($format);
							return $codes;
						}
					}
				}
			}
	}		
		
	private function serviceCode() 
	{
			if(Mage::getStoreConfig('rmde/misc/servicecode')=='')
			{
				return 'CRL01';
			}else{
				return strtoupper(Mage::getStoreConfig('rmde/misc/servicecode'));				
			}
	}
	
	public function activity($text)
		{
			if(Mage::getStoreConfig('rmde/advanced/activity')==1)
				{
					Mage::log($text); 
				}

		}	
		
	public function unSave($file)
		{							
			if(Mage::getStoreConfig('rmde/advanced/trbackup')!=1)
				{
					if(unlink($file))
						{
							$this->activity($file.' removed');
						}else{
							$this->activity('Unable to remove '.$file);
						}
				}
		}	




}
?>
