<?php
/**
 * Magento Mad Capsule Media Royal Mail Despatch Express Extension
 * http://www.madcapsule.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @copyright  Copyright (c) 2009 Mad Capsule Media (http://www.madcapsule.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     James Mikkelson <james@madcapsule.co.uk>
 * @filever 0.0.1
 *
*/
class MadCapsule_Rmde_Model_Consignment_Api extends Mage_Api_Model_Resource_Abstract
{
    public function getConsignment()
    {		
     	$transits = Mage::getModel("rmde/consignment")->getCollection();
    	$transits->addFieldToFilter('status', array('like' => '1'));

		$config = Mage::getStoreConfig('rmde');
    	$items = $transits->getItems();
		if(Mage::getStoreConfig('rmde/misc/useorderid')==true)
		{
			$label_id = 'order';
		}else{
			$label_id = 'shipment';
		}

		$consignments = array();
		foreach($items as $item)
		{
			$consignments[] = $item['shipment_id'].','.$item['service'].','.$item['weight'].','.$item['transfer_id'].','.$item['order_id'].','.$label_id.','.Mage::getStoreConfig('rmde/misc/printer').','.$item['format'];
		}
		
		return $consignments;
    }

    public function getConfiguration($setting)
    {
		try
		{
			return Mage::getStoreConfig('rmde/misc/'.$setting.'');	

		}
		catch(Exception $e)
		{
			//theres no error reporting available on courier connect so api errors will have to go to logfile
			Mage::log('There was a problem calling a setting via the API: '.$e.'',null,'madcapsule_system.log');
		}
    }

    public function receiveConsignment($shipment,$increment)
    {

		try
		{		
    		$consignment = Mage::getModel('rmde/consignment');
			$consignment->setTransferId($shipment);
			$consignment->setStatus(2);
			$consignment->save();
	    	$consignment = Mage::getModel('rmde/consignment');
			$consignment->setTransferId(1);
			$consignment->setConsignment($increment);
			$consignment->setShipmentId($shipment);
			$consignment->save();
			return true;
		}
		catch(Exception $e)
		{
			Mage::log('There was an error receiving a consignment: '.$e.'',null,'madcapsule_system.log');
		}
    }
	
	public function returnVersion()
    {
        return Mage::getVersion();
    }
	
	public function returnConsignment($reference,$status,$message)
	{
		$current_consignment = $this->currentShipment();
		if($current_consignment[0]=='current'){
			Mage::log('Tried to return an unknown shipment. So I died.',null,'madcapsule_system.log');
			return "This label was not expected in Magento.";
			exit();
		}
		try
		{
			$shipment = Mage::getModel("sales/order_shipment")->loadByIncrementId($current_consignment[0]);
			$consignment = Mage::getModel('rmde/consignment');
			$consignment->setTransferId($current_consignment[1]);
			if(Mage::getStoreConfig('rmde/misc/emailcustomer')==TRUE)
			{
				$customer = array();
				$customer['name'] = $shipment->getOrder()->getCustomerFirstname().' '.$shipment->getOrder()->getCustomerLastname();
				$customer['email'] = $shipment->getOrder()->getCustomerEmail();	
				$consignment->setEmail($customer['email']);
			}
			if($status==0 || $status==1){
				$consignment->setConsignment($reference);
				$consignment->setStatus(3);
			}else{
				$consignment->setStatus(4);
			}
			$consignment->setCode($status);
			$consignment->setCodeMsg($message);
			$consignment->setReturned(NOW());
			$consignment->save();
			if(Mage::getStoreConfig('rmde/advanced/activity')==1)
			{
				Mage::log('Shipment ID '.$current_consignment[0].' returned ('.$reference.')',null,'madcapsule_system.log'); 
			}			
		}
		catch(Exception $e)
		{
			Mage::log('Unable to update tracking for shipment id '.$current_consignment[0].' Error: '.$e,null,'madcapsule_system.log');
			return;
		}
		
        $track = Mage::getModel('sales/order_shipment_track')
                 ->setShipment($shipment)
                 ->setData('title', 'Royal Mail')
                 ->setData('number', $reference)
                 ->setData('carrier_code', 'custom')
                 ->setData('order_id', $shipment->getData('order_id'))
                 ->save();		
		
				
		if($track && Mage::getStoreConfig('rmde/advanced/activity')==1)
		{
			Mage::log('Tracking added: '.$reference.' for shipment #'.$current_consignment[0],null,'madcapsule_system.log'); 
		}

		if(Mage::getStoreConfig('rmde/misc/emailcustomer')==TRUE)
		{
			
			$emailTemplate  = Mage::getModel('core/email_template')
				->loadDefault('madcapsule_rmde_tracking_template');									
				
			$emailTemplateVariables = array();
			$emailTemplateVariables['consignment'] = $reference;

			$emailTemplate->setTemplateSubject('Your order has been dispatched');
			$storeEmail = Mage::getStoreConfig('trans_email/ident_general/email');
			$storeContact = Mage::getStoreConfig('trans_email/ident_general/name'); 
			$emailTemplate->setSenderEmail($storeEmail);
			$emailTemplate->setSenderName($storeContact);
			$emailTemplate->send($customer['email'],$customer['name'], $emailTemplateVariables);
			if(Mage::getStoreConfig('rmde/advanced/activity')==1)
			{
				Mage::log('Royal Mail tracking email sent to '.$customer['email'].'',null,'madcapsule_system.log'); 
			}
		}
		if($saved){
			try{
				$consignment = Mage::getModel('rmde/consignment');
				$consignment->setTransferId(1);
				$consignment->setConsignment('current');
				$consignment->setShipmentId(NULL);
				$consignment->save();
			}catch(Exception $e)
			{
				Mage::log('Failed to reset current consignment',null,'madcapsule_system.log');
				return;
			}
		}
	}
	
	private function currentShipment()
		{
			
			if(Mage::getStoreConfig('rmde/advanced/current')>=1)
				{
						$current = Mage::getStoreConfig('rmde/advanced/current');
				}else{
						$current = 1;
				}
			$shipment = Mage::getModel("rmde/consignment")->load($current);
			return array($shipment->getConsignment(),$shipment->getShipmentId());
		}

}
?>
