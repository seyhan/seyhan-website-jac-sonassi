<?php
/**
 * Magento Mad Capsule Media Royal Mail Despatch Express Extension
 * http://www.madcapsule.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @copyright  Copyright (c) 2009 Mad Capsule Media (http://www.madcapsule.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     James Mikkelson <james@madcapsule.co.uk>
*/
class MadCapsule_Rmde_DeliveryOptions
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'CRL01', 'label'=>Mage::helper('adminhtml')->__('Packet Post Daily Rate 1st Class')),
            array('value' => 'CRL02', 'label'=>Mage::helper('adminhtml')->__('Packet Post Daily Rate 2nd Class')),
            array('value' => 'FS101', 'label'=>Mage::helper('adminhtml')->__('Packetsort 8 1st Class Flat Rate Large Letter')),
            array('value' => 'FS102', 'label'=>Mage::helper('adminhtml')->__('Packetsort 8 2nd Class Flat Rate Large Letter')),
            array('value' => 'MP101', 'label'=>Mage::helper('adminhtml')->__('International Coontract Airsure')),
            array('value' => 'MP401', 'label'=>Mage::helper('adminhtml')->__('International Contract Airsure w/ Compensation')),
            array('value' => 'MP501', 'label'=>Mage::helper('adminhtml')->__('International Contract Signed')),
            array('value' => 'MP601', 'label'=>Mage::helper('adminhtml')->__('International Contract Signed w/ Compensation')),
            array('value' => 'OF101', 'label'=>Mage::helper('adminhtml')->__('International Format Sort Priority')),
            array('value' => 'OF301', 'label'=>Mage::helper('adminhtml')->__('International Format Sort Economy')),
            array('value' => 'OF401', 'label'=>Mage::helper('adminhtml')->__('International Format Sort Priority Mechable')),
            array('value' => 'OF601', 'label'=>Mage::helper('adminhtml')->__('International Format Sort Economy Mechable')),
            array('value' => 'OLA01', 'label'=>Mage::helper('adminhtml')->__('Overseas Letters Airmail')),
            array('value' => 'OLS01', 'label'=>Mage::helper('adminhtml')->__('Overseas Letter Surface')),
            array('value' => 'OZ101', 'label'=>Mage::helper('adminhtml')->__('International Zone Sort Priority')),
            array('value' => 'OZ301', 'label'=>Mage::helper('adminhtml')->__('International Zone Sort Economy')),
            array('value' => 'OZ401', 'label'=>Mage::helper('adminhtml')->__('International Zone Sort Priority Mechinable')),
            array('value' => 'OZ601', 'label'=>Mage::helper('adminhtml')->__('International Zone Sort Economy Mechinable')),
            array('value' => 'PK001', 'label'=>Mage::helper('adminhtml')->__('Packetpost Large Letter Flat Rate 2nd Class')),
            array('value' => 'PK101', 'label'=>Mage::helper('adminhtml')->__('Packetsort 8 1st Class Flat Rate Packets')),
            array('value' => 'PK201', 'label'=>Mage::helper('adminhtml')->__('Packetsort 8 2nd Class Flat Rate Packets')),
            array('value' => 'PK301', 'label'=>Mage::helper('adminhtml')->__('Packetsort 8 1st Class Daily Rate Packets')),
            array('value' => 'PK401', 'label'=>Mage::helper('adminhtml')->__('Packetsort 8 2nd Class Daily Rate Packets')),
            array('value' => 'PPF01', 'label'=>Mage::helper('adminhtml')->__('Packetpost Flat Rate 1st Class')),
            array('value' => 'PPF02', 'label'=>Mage::helper('adminhtml')->__('Packetpost Flat Rate 2nd Class')),
            array('value' => 'PK901', 'label'=>Mage::helper('adminhtml')->__('Packetpost Large Letter Flat Rate 1st Class')),
            array('value' => 'STL01', 'label'=>Mage::helper('adminhtml')->__('Standard Tariff Letters 1st Class')),
            array('value' => 'STL02', 'label'=>Mage::helper('adminhtml')->__('Standard Tariff Letters 2nd Class')),
            array('value' => 'SD101', 'label'=>Mage::helper('adminhtml')->__('Special Delivery Next Day 500GBP')),
            array('value' => 'SD201', 'label'=>Mage::helper('adminhtml')->__('Special Delivery Next Day 1000GBP')),
            array('value' => 'SD301', 'label'=>Mage::helper('adminhtml')->__('Special Delivery Next Day 2500GBP')),
            array('value' => 'SD401', 'label'=>Mage::helper('adminhtml')->__('Special Delivery 9AM 50GBP')),
            array('value' => 'SD501', 'label'=>Mage::helper('adminhtml')->__('Special Delivery 9AM 1000GBP')),
            array('value' => 'SD601', 'label'=>Mage::helper('adminhtml')->__('Special Delivery 9AM 2500GBP')),
			array('value' => 'TPN01', 'label'=>Mage::helper('adminhtml')->__('Tracked Next Day')),
            array('value' => 'TP201', 'label'=>Mage::helper('adminhtml')->__('Royal Mail Tracked')),
            array('value' => 'IE101', 'label'=>Mage::helper('adminhtml')->__('International Packets Zone Sort Priority')),
            array('value' => 'IE301', 'label'=>Mage::helper('adminhtml')->__('International Packets Zone Sort Economy')),
            array('value' => 'IG101', 'label'=>Mage::helper('adminhtml')->__('International Flats Zone Sort Priority')),
            array('value' => 'IG301', 'label'=>Mage::helper('adminhtml')->__('International Flats Zone Sort Economy')),
            array('value' => 'IG401', 'label'=>Mage::helper('adminhtml')->__('International Flats Zone Sort Priority Mechinable')),
            array('value' => 'IG601', 'label'=>Mage::helper('adminhtml')->__('International Flats Zone Sort Economy Mechinable')),
            array('value' => 'IP101', 'label'=>Mage::helper('adminhtml')->__('International Letters Zone Sort Priority')),
            array('value' => 'IP301', 'label'=>Mage::helper('adminhtml')->__('International Letters Zone Sort Economy')),
            array('value' => 'IP401', 'label'=>Mage::helper('adminhtml')->__('International Letters Zone Sort Priority Machinable')),
            array('value' => 'IP601', 'label'=>Mage::helper('adminhtml')->__('International Letters Zone Sort Economy Machinable')),
            array('value' => 'WE101', 'label'=>Mage::helper('adminhtml')->__('International Packets Unsorted Priority')),
            array('value' => 'WE301', 'label'=>Mage::helper('adminhtml')->__('International Packets Unsorted Economy')),
            array('value' => 'WG101', 'label'=>Mage::helper('adminhtml')->__('International Flats Unsorted Priority')),
            array('value' => 'WG301', 'label'=>Mage::helper('adminhtml')->__('International Flats Economy Priority')),
            array('value' => 'WG401', 'label'=>Mage::helper('adminhtml')->__('International Flats Unsorted Priority Machinable')),
            array('value' => 'WG601', 'label'=>Mage::helper('adminhtml')->__('International Flats Unsorted Economy Machinable')),
            array('value' => 'WP101', 'label'=>Mage::helper('adminhtml')->__('International Letters Unsorted Priority'))
        );
    }
}
