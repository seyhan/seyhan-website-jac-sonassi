<?php
/**
 * Magento Mad Capsule Media Royal Mail Despatch Express Extension
 * http://www.madcapsule.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @copyright  Copyright (c) 2009 Mad Capsule Media (http://www.madcapsule.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     James Mikkelson <james@madcapsule.co.uk>
*/
class MadCapsule_Rmde_SalesShipmentController extends Mage_Adminhtml_Controller_Sales_Shipment
{

  public function queueAction()
  {
	$shipmentIds = $this->getRequest()->getParam('shipment_ids');

	if(Mage::getStoreConfig('rmde/misc/servicecode')=='')
	{
		$serviceCode =  'CRL01';
	}else{
		$serviceCode =  strtoupper(Mage::getStoreConfig('rmde/misc/servicecode'));				
	}
	
	
	if(Mage::getStoreConfig('rmde/misc/format')=='')
	{
		$format =  'P';
	}else{
		$format =  strtoupper(Mage::getStoreConfig('rmde/misc/format'));				
	}

	foreach($shipmentIds as $shipmentId)
		{
		
		    $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
         	$shipment_details = $shipment->getIncrementId();
			$_order = Mage::getModel('sales/order')->load($shipment->getOrderId());
			$order_inc_id = $_order->getIncrementId();
    		$consignment = Mage::getModel('rmde/consignment');
			$consignment->setShipmentId($shipment_details);
			$consignment->setFormat($format);
			$consignment->setOrderId($order_inc_id);
			$consignment->setStatus(1);
			$consignment->setService($serviceCode);
			$consignment->setWeight(Mage::helper('rmde')->getWeight($shipment_details));
			$consignment->save();
		}
   	
	$this->_getSession()->addSuccess($this->__("".count($shipmentIds)." Consignments queued to be sent to Despatch Express."));
    	$this->_redirect('adminhtml/sales_shipment/');
  }
}
?>
