<?php
/**
 * Magento Mad Capsule Media RMDMO Extension
 * http://www.madcapsule.com
 *

 * @copyright  Copyright (c) 2013 Mad Capsule Media (http://www.madcapsule.com)
 * @license    Commercial. Contact author prior to use. No replication or redistribution permitted.
 * @author     James Mikkelson <james@madcapsule.co.uk>
*/
class MadCapsule_Rmde_Modes
{
    public function toOptionArray()
    {
        return array(
				array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('Manual Override')),
				array('value' => 2, 'label'=>Mage::helper('adminhtml')->__('Automatic with Default')),
				array('value' => 3, 'label'=>Mage::helper('adminhtml')->__('Automatic with Mapping'))
        );
    }
}
