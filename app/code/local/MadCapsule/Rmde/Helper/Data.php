<?php
/**
 * Magento Mad Capsule Media Royal Mail Despatch Express  Extension
 * http://www.madcapsule.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @copyright  Copyright (c) 2009 Mad Capsule Media (http://www.madcapsule.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     James Mikkelson <james@madcapsule.co.uk>
*/

class MadCapsule_Rmde_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getWeight($shipment)
		{
			$shipmentWeight = 0;
			$shipment = Mage::getModel("sales/order_shipment")->loadByIncrementId($shipment);
        		foreach ($shipment->getItemsCollection() as $item) {
            			if (!$item->getOrderItem()->getParentItem()) {
						$shipmentWeight = $shipmentWeight + ($item->getWeight()*$item->getQty());
            			}
        		}
			return $shipmentWeight;
		}
		
	public function niceServiceNames($service)
		{
			$serviceNames = array(
						'CRL01' => 'Packet Post Daily Rate 1st Class',
						'CRL02' => 'Packet Post Daily Rate 2nd Class',
						'FS101' => 'Packetsort 8 1st Class Flat Rate Large Letter',
						'FS102' => 'Packetsort 8 2nd Class Flat Rate Large Letter',
						'MP101' => 'International Coontract Airsure',
						'MP401' => 'International Contract Airsure w/ Compensation',
						'MP501' => 'International Contract Signed',
						'MP601' => 'International Contract Signed w/ Compensation',
						'OF101' => 'International Format Sort Priority',
						'OF301' => 'International Format Sort Economy',
						'OF401' => 'International Format Sort Priority Mechable',
						'OF601' => 'International Format Sort Economy Mechable',
						'OLA01' => 'Overseas Letters Airmail',
						'OLS01' => 'Overseas Letter Surface',
						'OZ101' => 'International Zone Sort Priority',
						'OZ301' => 'International Zone Sort Economy',
						'OZ401' => 'International Zone Sort Priority Mechinable',
						'OZ601' => 'International Zone Sort Economy Mechinable',
						'PK001' => 'Packetpost Large Letter Flat Rate 2nd Class',
						'PK101' => 'Packetsort 8 1st Class Flat Rate Packets',
						'PK201' => 'Packetsort 8 2nd Class Flat Rate Packets',
						'PK301' => 'Packetsort 8 1st Class Daily Rate Packets',
						'PK401' => 'Packetsort 8 2nd Class Daily Rate Packets',
						'PPF01' => 'Packetpost Flat Rate 1st Class',
						'PPF02' => 'Packetpost Flat Rate 2nd Class',
						'PK901' => 'Packetpost Large Letter Flat Rate 1st Class',
						'STL01' => 'Standard Tariff Letters 1st Class',
						'STL02' => 'Standard Tariff Letters 2nd Class',
						'SD101' => 'Special Delivery Next Day 500GBP',
						'SD201' => 'Special Delivery Next Day 1000GBP',
						'SD301' => 'Special Delivery Next Day 2500GBP',
						'SD401' => 'Special Delivery 9AM 50GBP',
						'SD501' => 'Special Delivery 9AM 1000GBP',
						'SD601' => 'Special Delivery 9AM 2500GBP',
						'TP201' => 'Royal Mail Tracked',
						'IE101' => 'International Packets Zone Sort Priority',
						'IE301' => 'International Packets Zone Sort Economy',
						'IG101' => 'International Flats Zone Sort Priority',
						'IG301' => 'International Flats Zone Sort Economy',
						'IG401' => 'International Flats Zone Sort Priority Mechinable',
						'IG601' => 'International Flats Zone Sort Economy Mechinable',
						'IP101' => 'International Letters Zone Sort Priority',
						'IP301' => 'International Letters Zone Sort Economy',
						'IP401' => 'International Letters Zone Sort Priority Machinable',
						'IP601' => 'International Letters Zone Sort Economy Machinable',
						'WE101' => 'International Packets Unsorted Priority',
						'WE301' => 'International Packets Unsorted Economy',
						'WG101' => 'International Flats Unsorted Priority',
						'WG301' => 'International Flats Economy Priority',
						'WG401' => 'International Flats Unsorted Priority Machinable',
						'WG601' => 'International Flats Unsorted Economy Machinable',
						'WP101' => 'International Letters Unsorted Priority'
					     );	
			return $serviceNames[$service];

		}

	public function formats()
		{
			$formats = array('P','L','F');	
			return $formats;

		}
}

