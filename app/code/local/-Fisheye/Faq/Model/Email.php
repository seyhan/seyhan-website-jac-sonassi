<?php

class Fisheye_Faq_Model_Email extends Mage_Core_Model_Abstract
{


    public function generate($html, $vars)
    {
        foreach ($vars as $i => $k) {
            $html = str_replace("{{" . $i . "}}", $k, $html);
        }
        return $html;
    }


    public function sendAdminEmail($vars)
    {
        $address = Mage::getStoreConfig('trans_email/ident_general/email');

        $template = 'Dear Admin,<br>{{customer_name}} ({{customer_email}}) has asked the following question on your store.<br><BR>"{{question}}". <BR><BR>Please login to the Admin Area FAQ section to respond to this question.';
        $subject = "New FAQ Question Posted";
        $email = $this->generate($template, $vars);

        $fromEmail = Mage::getStoreConfig("faq/options/emailfrom");
        $fromName = Mage::getStoreConfig("faq/options/emailname");

        $toEmail = $address;
        $toName = $address;

        $body = "This is Test Email!"; // body text
        $subject = "Test Subject"; // subject text

        $mail = new Zend_Mail();

        $mail->setBodyHtml($email);

        $mail->setFrom($fromEmail, $fromName);

        $mail->addTo($toEmail, $toName);

        $mail->setSubject($this->generate($subject, $vars));
        $mail->send();

    }


public function sendNotifyEmail($address, $vars)
{
    if ($vars['prod_name'] == "") {
        $template = Mage::getStoreConfig("faq/options/general_template");
    } else {
        $template = Mage::getStoreConfig("faq/options/product_template");
    }
    $subject = Mage::getStoreConfig("faq/options/subject");
    $email = $this->generate($template, $vars);

     $fromEmail = Mage::getStoreConfig("faq/options/emailfrom");
        $fromName = Mage::getStoreConfig("faq/options/emailname");

        $toEmail = $address;
        $toName = $address;

        $body = "This is Test Email!"; // body text
        $subject = "Test Subject"; // subject text

        $mail = new Zend_Mail();

        $mail->setBodyHtml($email);

        $mail->setFrom($fromEmail, $fromName);

        $mail->addTo($toEmail, $toName);

        $mail->setSubject($this->generate($subject, $vars));
        $mail->send();

}


}
