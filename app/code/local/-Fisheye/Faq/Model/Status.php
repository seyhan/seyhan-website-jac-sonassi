<?php

class Fisheye_Faq_Model_Status extends Varien_Object
{
    static public function getVisibility()
    {
        return array(0 => Mage::helper('faq')->__('Awaiting Admin Read'), 
        1 => Mage::helper('faq')->__('General and Product Page'), 
        2 => Mage::helper('faq')->__('General Page Only'),
            3 => Mage::helper('faq')->__('Product Page Only'), 
            4 => Mage::helper('faq')->__('Declined'),
            5 => Mage::helper('faq')->__('Private (Not Published)'));

    }


    static public function getProducts()
    {
        $products = array();

        $products[0] = "General Question";

        $collection = Mage::getModel("catalog/product")->getCollection()->
            addAttributeToSelect(array('name', 'sku'));

        foreach ($collection as $i => $k) {
            $products[$k->getId()] = Mage::helper('faq')->__($k->getName() . "(" .
                strtoupper($k->getSku()) . ")");
        }

        return $products;
    }


}
