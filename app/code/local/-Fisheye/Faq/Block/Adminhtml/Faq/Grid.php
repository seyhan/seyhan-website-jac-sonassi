<?php

class Fisheye_Faq_Block_Adminhtml_Faq_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('faqGrid');
      $this->setDefaultSort('faq_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('faq/faq')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('faq_id', array(
          'header'    => Mage::helper('faq')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'faq_id',
      ));

      $this->addColumn('question', array(
          'header'    => Mage::helper('faq')->__('Question'),
          'align'     =>'left',
          'index'     => 'question',
      ));


   $this->addColumn('answer', array(
          'header'    => Mage::helper('faq')->__('Answer'),
          'align'     =>'left',
          'index'     => 'answer',
      ));




$products = Mage::getSingleton('faq/status')->getProducts();


/* End Build Product Collection */

      $this->addColumn('product', array(
          'header'    => Mage::helper('faq')->__('Product'),
          'align'     => 'left',
          'width'     => '250px',
          'index'     => 'product',
          'type'      => 'options',
          'options'   => $products,
      ));

     
         $this->addColumn('customer_email', array(
          'header'    => Mage::helper('faq')->__('Customer Email Address'),
          'align'     =>'left',
          'index'     => 'customer_email',
      ));
      
     
    $statuses = Mage::getSingleton('faq/status')->getVisibility();

         $this->addColumn('status', array(
          'header'    => Mage::helper('faq')->__('Visibility'),
          'align'     => 'left',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => $statuses
      ));
      
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('faq')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('faq')->__('Edit Question'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
	//	$this->addExportType('*/*/exportCsv', Mage::helper('faq')->__('CSV'));
	//	$this->addExportType('*/*/exportXml', Mage::helper('faq')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('faq_id');
        $this->getMassactionBlock()->setFormFieldName('faq');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('faq')->__('Delete Selected Questions'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('faq')->__('Are you sure you want to delete this FAQ Questions?')
        ));


        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}