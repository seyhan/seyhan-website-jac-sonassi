<?php

class Fisheye_Faq_Block_Adminhtml_Faq_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('faq_form', array('legend'=>Mage::helper('faq')->__('Question Manager')));
     
      $fieldset->addField('question', 'editor', array(
            'label'     => Mage::helper('faq')->__('Question'),
          'title'     => Mage::helper('faq')->__('Question'),
          'style'     => 'width:600px; height:110px;',
          'wysiwyg'   => false,
           'name'      => 'question',
          'required'  => true,
      ));

      $fieldset->addField('answer', 'editor', array(
       'label'     => Mage::helper('faq')->__('Answer'),
          'title'     => Mage::helper('faq')->__('Answer'),
          'style'     => 'width:600px; height:110px;',
          'wysiwyg'   => false,
           'name'      => 'answer',
          'required'  => true,
	  ));
      
      
          $fieldset->addField('customer_email', 'text', array(
          'label'     => Mage::helper('faq')->__('Customers Email Address'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'customer_email',
	  ));
   
   $fieldset->addField('customer_name', 'text', array(
          'label'     => Mage::helper('faq')->__('Customers Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'customer_name',
	  ));
      
      
	$visibility = Mage::getSingleton('faq/status')->getVisibility(); 
       $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('faq')->__('Status'),
          'name'      => 'status',
          'values'    => $visibility
       ));
       
      
      	$products = Mage::getSingleton('faq/status')->getProducts();
            $fieldset->addField('product', 'select', array(
          'label'     => Mage::helper('faq')->__('Product'),
          'name'      => 'product',
          'values'    => $products
      ));
      
      
     $fieldset->addField('customer_notify', 'checkbox', array(
          'name'      => 'customer_notify',
          'label'     => Mage::helper('faq')->__('Email Customer Response'),
          'value'     => 1,
          'after_element_html' => Mage::helper('faq')->__(' If this is ticked the customer will recieve an email with this answer and question')
      ));
      
     
      
          $fieldset->addField('order_by', 'text', array(
       'label'     => Mage::helper('faq')->__('Question Run Order'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'order_by',
	  ));
      
      
      
      
      if ( Mage::getSingleton('adminhtml/session')->getFaqData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getFaqData());
          Mage::getSingleton('adminhtml/session')->setFaqData(null);
      } elseif ( Mage::registry('faq_data') ) {
          $form->setValues(Mage::registry('faq_data')->getData());
      }
      return parent::_prepareForm();
  }
}