<?php

$installer = $this;

$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS `fisheye_faq` (
  `faq_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `product` int(11) NOT NULL,
  `customer_email` varchar(500) NOT NULL,
  `customer_name` varchar(500) NOT NULL,
  `status` int(11) NOT NULL,
  `order_by` int(11) NOT NULL,
  `created_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

");

$installer->endSetup(); 