<?php

class Fisheye_Faq_IndexController extends Mage_Core_Controller_Front_Action
{


    public function askAction()
    {
        if ($this->getRequest()->getPost()) {
		$postData = $this->getRequest()->getPost();
		if (isset($postData['question'])) {
		  $q = $postData['question']; 
		  if($q != strip_tags($postData['question'])){
		   	Mage::getSingleton('core/session')->addError('Please post question without HTML.');
		   }
		   else{ 
		   /* Ok now lets create the question */

            $post = $this->getRequest()->getPost();
            $post['status'] = 0;
            $post['created_time'] = date("Y-m-d G:i:s");
            $post['update_time'] = date("Y-m-d G:i:s");
            $post['order_by'] = "100000";
            $model = Mage::getModel("faq/faq");
            $model->setData($post);
            $model->save();


            /* Send Email To Admin */

            //Mage::getModel("faq/email")->sendAdminEmail($post);

            Mage::getSingleton('core/session')->addSuccess('Thankyou for sending us a question, We will reply to your question by email to the address: ' .
                $post['customer_email']);
		   }
		}	

           

        } else {

            Mage::getSingleton('core/session')->addError('Your question could not be asked at this time.');

        }

        $this->_redirectReferer();
    }


    public function indexAction()
    {
        /* Sites Main FAQ Section	*/
        $this->loadLayout()->renderLayout();
    }
}