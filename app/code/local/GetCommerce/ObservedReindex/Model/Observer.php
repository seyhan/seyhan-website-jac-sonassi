<?php
/**
 *
 *
 * @author  Ross Knight (getcommerce.com)
 * @copyright  Copyright (c) 2015 GetCommerce
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
class GetCommerce_ObservedReindex_Model_Observer
{
    /**
     * Prepare product for duplicate action.
     *
     * @param $observer
     */
    public function fixPrice($observer)
    {
        // E Starter Kits id is 127
        $ids = array(127, 30, 159);
        $compairCount = 0;

        foreach($ids as $id) {
            if(false == $this->compare(127)) {
                $compairCount++;
            }
        }

        if ($compairCount > 0) {
           $this->runIndexer();
        } else {
            Mage::Log('GetCommerce indexer not required to run');
        }
    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function getCategoryIndexSize($id)
    {
        // E Starter Kits id is 127
        $dbRead = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $dbRead->query("SELECT `e`.*, `cat_index`.`position` AS `cat_index_position`, `price_index`.`price`, `price_index`.`tax_class_id`, `price_index`.`final_price`, IF(price_index.tier_price IS NOT NULL, LEAST(price_index.min_price, price_index.tier_price), price_index.min_price) AS `minimal_price`, `price_index`.`min_price`, `price_index`.`max_price`, `price_index`.`tier_price` FROM `catalog_product_entity` AS `e` INNER JOIN `catalog_category_product_index` AS `cat_index` ON cat_index.product_id=e.entity_id AND cat_index.store_id=1 AND cat_index.visibility IN(2, 4) AND cat_index.category_id='{$id}' AND cat_index.is_parent=1 INNER JOIN `catalog_product_index_price` AS `price_index` ON price_index.entity_id = e.entity_id AND price_index.website_id = '1' AND price_index.customer_group_id = 0 ORDER BY `cat_index`.`position` ASC LIMIT 30");
        return $sql->rowCount();
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function compare($id)
    {
        $indexSize = $this->getCategoryIndexSize($id);
        $collectionSize = $this->getCategoryCollectionSize($id);
        $matches = false;
        if($collectionSize == $indexSize) {
            $matches = true;
        }
        return $matches;
    }

    public function getCategoryCollectionSize($id)
    {
        $size = Mage::getModel('catalog/category')->load($id)
            ->getProductCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', 1)
            ->addAttributeToFilter('visibility', 4)
            ->getSize();
        return $size;
    }

    public function runIndexer()
    {
        try {
            $process = Mage::getModel('index/process')->load(2);
            $process->reindexAll();
            Mage::Log("GetCommerce extension reindexed Prices");
        }
        catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}