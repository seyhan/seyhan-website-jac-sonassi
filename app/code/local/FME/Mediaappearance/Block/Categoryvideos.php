<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/ 
class FME_Mediaappearance_Block_Categoryvideos extends Mage_Core_Block_Template 
{

    public function getCategoryvideos($limit = 5)     
	{ 
		$mediaTable 		 = Mage::getSingleton('core/resource')->getTableName('mediaappearance');
		$mediaStoreTable 	 = Mage::getSingleton('core/resource')->getTableName('media_store');
		$mediaCategoryTable  = Mage::getSingleton('core/resource')->getTableName('media_category_video');
	
		$storeId = Mage::app()->getStore()->getStoreId(); 
		$cid = Mage::registry('current_category')->getId();
		$sqry = "select * from ".$mediaTable." m
				 inner join ".$mediaStoreTable." ms on ms.mediaappearance_id = m.mediaappearance_id
				 inner join ".$mediaCategoryTable." mc on mc.mediaappearance_id = m.mediaappearance_id
				 where mc.category_id = '".$cid."'  
				 and ms.store_id in (0,".$storeId.")
				 and m.status = 1
				 order by m.created_time DESC";
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		$select = $connection->query($sqry);
		$collection = $select->fetchAll();
		return $collection;
        
	}
	
	public function getPageLayout()
    {
        return $this->helper('page/layout')->getCurrentPageLayout();
    }

}