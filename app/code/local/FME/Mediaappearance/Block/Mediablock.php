<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/  
 
class FME_Mediaappearance_Block_Mediablock extends Mage_Catalog_Block_Product_Abstract
{ 
	
	protected function _tohtml()
    {	
	 
	    $blockidentifier = $this->getBlockId();
		
				
        $this->setTemplate("mediaappearance/mediablock.phtml");
		return parent::_toHtml();
		
    }
	
}
