<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Mediaappearance 
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/ 

class FME_Mediaappearance_Block_Adminhtml_Videoblocks_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{ 
  protected function _prepareForm()
  {
	$form = new Varien_Data_Form();
	$this->setForm($form);
	$fieldset = $form->addFieldset('videoblocks_form', array('legend'=>Mage::helper('mediaappearance')->__('Media Block information')));
	
	$fieldset->addField('block_title', 'text', array(
	  'label'     => Mage::helper('mediaappearance')->__('Title'),
	  'class'     => 'required-entry',
	  'required'  => true,
	  'name'      => 'block_title',
	));
	
	$fieldset->addField('block_identifier', 'text', array(
		'name'      => 'block_identifier',
		'label'     => Mage::helper('mediaappearance')->__('Identifier'),
		'title'     => Mage::helper('mediaappearance')->__('Identifier'),
		'required'  => true,
		'class'     => 'validate-identifier',
		//'after_element_html' => '<p class="nm"><small>' . Mage::helper('faqs')->__('(eg: domain.com/faqs/identifier)') . '</small></p>',
	));
	
	$fieldset->addField('block_status', 'select', array(
	  'label'     => Mage::helper('mediaappearance')->__('Status'),
	  'name'      => 'block_status',
	  'values'    => array(
		  array(
			  'value'     => 1,
			  'label'     => Mage::helper('mediaappearance')->__('Enabled'),
		  ),
	
		  array(
			  'value'     => 2,
			  'label'     => Mage::helper('mediaappearance')->__('Disabled'),
		  ),
	  ),
	));
	
	$fieldset->addField('block_area', 'select', array(
	  'label'     => Mage::helper('mediaappearance')->__('Block Display Area'),
	  'name'      => 'block_area',
	  'values'    => array(
		  array(
			  'value'     => 'side',
			  'label'     => Mage::helper('mediaappearance')->__('Left / Right'),
		  ),
	
		  array(
			  'value'     => 'main',
			  'label'     => Mage::helper('mediaappearance')->__('Main'),
		  ),
	  ),
	));
	
	try{
	$config = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
		array(
				'add_widgets' => false,
				'add_variables' => false,
			)
		);
	$config->setData(Mage::helper('mediaappearance')->recursiveReplace(
				'/mediaappearance/',
				'/'.(string)Mage::app()->getConfig()->getNode('admin/routers/adminhtml/args/frontName').'/',
				$config->getData()
			)
		);
	}
	catch (Exception $ex){
	$config = null;
	}
	
	$fieldset->addField('block_content', 'editor', array(
	  'name'      => 'block_content',
	  'label'     => Mage::helper('mediaappearance')->__('Content'),
	  'title'     => Mage::helper('mediaappearance')->__('Content'),
	  'style'     => 'width:760px; height:500px;',
	  'config'    => $config	  
	));
	  
      
      if ( Mage::getSingleton('adminhtml/session')->getVideoblocksData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getVideoblocksData());
          Mage::getSingleton('adminhtml/session')->setVideoblocksData(null);
      } elseif ( Mage::registry('media_block_data') ) {
          $form->setValues(Mage::registry('media_block_data')->getData());
      }
      return parent::_prepareForm();
  }
}