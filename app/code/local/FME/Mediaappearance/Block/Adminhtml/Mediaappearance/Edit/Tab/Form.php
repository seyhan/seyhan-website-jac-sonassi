<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/

class FME_Mediaappearance_Block_Adminhtml_Mediaappearance_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
	  
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('mediaappearance_form', array('legend'=>Mage::helper('mediaappearance')->__('Media information')));
		
		$object = Mage::getModel('mediaappearance/mediaappearance')->load( $this->getRequest()->getParam('id') );
		$videoURL   = false;
	  
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('mediaappearance')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
	  
	  $field = 	$fieldset->addField('video', 'hidden', array(
		  'label'     => Mage::helper('mediaappearance')->__(''),
		  'name'      => 'video',
		  'id'      => 'video',
		  
	  ));
	  
	  $field->setRenderer($this->getLayout()->createBlock('mediaappearance/adminhtml_mediaappearance_edit_renderer_video'));
	  
	$fieldset->addField('my_file', 'hidden', array(
		'name'        => 'my_file',
	));
	$fieldset->addField('my_thumb', 'hidden', array(
		'name'        => 'my_thumb',
	));

	  
	  $fieldset->addField('product_ids', 'hidden', array(
          'label'     => Mage::helper('mediaappearance')->__(''),
          'name'      => 'product_ids',
	  	  'id'      => 'product_ids',
          
      ));
	  
	  $fieldset->addField('featured_video', 'select', array(
          'label'     => Mage::helper('mediaappearance')->__('Featured video ?'),
          'name'      => 'featured_video',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('mediaappearance')->__('Yes'),
              ),

              array(
                  'value'     => 0,
                  'label'     => Mage::helper('mediaappearance')->__('No'),
              ),
          ),
      ));
	  
	  $fieldset->addField('show_in_right_column', 'select', array(
          'label'     => Mage::helper('mediaappearance')->__('Enable for right block ?'),
          'name'      => 'show_in_right_column',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('mediaappearance')->__('Yes'),
              ),

              array(
                  'value'     => 0,
                  'label'     => Mage::helper('mediaappearance')->__('No'),
              ),
          ),
      ));
	    
	  $fieldset->addField('store_id','multiselect',array(
			'name'      => 'stores[]',
            'label'     => Mage::helper('mediaappearance')->__('Store View'),
            'title'     => Mage::helper('mediaappearance')->__('Store View'),
            'required'  => true,
			'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true)
	   ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('mediaappearance')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('mediaappearance')->__('Enabled'),
              ),

              array(
                  'value'     => 0,
                  'label'     => Mage::helper('mediaappearance')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('mediaappearance')->__('Content'),
          'title'     => Mage::helper('mediaappearance')->__('Content'),
          'style'     => 'width:500px; height:300px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getMediaappearanceData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getMediaappearanceData());
          Mage::getSingleton('adminhtml/session')->setMediaappearanceData(null);
      } elseif ( Mage::registry('mediaappearance_data') ) {
          $form->setValues(Mage::registry('mediaappearance_data')->getData());
      }
      return parent::_prepareForm();
  }
}