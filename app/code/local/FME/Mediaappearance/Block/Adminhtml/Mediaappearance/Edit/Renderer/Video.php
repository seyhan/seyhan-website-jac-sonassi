<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/   

class FME_Mediaappearance_Block_Adminhtml_Mediaappearance_Edit_Renderer_Video extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface
{

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
		  
		$_val = Mage::registry('mediaappearance_data');
		$_Typevalfile = '';
		$_Typevalurl  = ''; 
		
		if($_val["mediatype"] == '1') {
			$_Typevalfile = 'checked="checked"';
			$_Typevalurl  = '';
		} elseif($_val["mediatype"] == '2') {
			$_Typevalurl = 'checked="checked"';
			$_Typevalfile = '';
		} else {
			$_Typevalfile = 'checked="checked"';
			$_Typevalurl = '';
		}
		
		//Get the Current File
		try {
			$object = Mage::getModel('mediaappearance/mediaappearance')->load( $this->getRequest()->getParam('id') );
			$note = false;
			
			$popupWidth  = Mage::getStoreConfig('mediaappearance/popupsettings/width');
			$popupHeight = Mage::getStoreConfig('mediaappearance/popupsettings/height');
			$autoPlay 	 = Mage::getStoreConfig('mediaappearance/popupsettings/autoplay');
			$playAgain 	 = Mage::getStoreConfig('mediaappearance/popupsettings/playagain');
						
			if($object["mediatype"] == "1") {
				
				if($object["filethumb"] != "") {
					$imgURL = Mage::getBaseUrl('media').$object["filethumb"];
				} else {
					$imgURL = Mage::getBaseUrl('media')."mediaappearance/video_icon_full.jpg";
				}
				$videoURL = Mage::getBaseUrl('media').$object["filename"];
				$videoRel = 'shadowbox;height='.$popupHeight.';width='.$popupWidth.';options={flashVars:{skin: '.Mage::getBaseUrl('js').'mediaappearance/skin01.zip,autostart: '.$autoPlay.'}}';
				
			} elseif ($object["mediatype"] == "2") {
				
				//For Thumb
				$videoURL = $object["videourl"];
				$videoData = Mage::helper('mediaappearance')->video_info($object["videourl"]);
				if ($videoData!==false) {
					if(Mage::getStoreConfig('mediaappearance/general/useyoutubethumb')) {
						$imgURL = $videoData['thumb_large'];	
					} else {
						if($object["filethumb"] != "") {
							$imgURL = Mage::getBaseUrl('media').$object["filethumb"];
						} else {
							$imgURL = Mage::getBaseUrl('media')."mediaappearance/video_icon_full.jpg";
						}
					}
				} else {
					if($object["filethumb"] != "") {
						$imgURL = Mage::getBaseUrl('media').$object["filethumb"];
					} else {
						$imgURL = Mage::getBaseUrl('media')."mediaappearance/video_icon_full.jpg";
					}
				}
				
				//For Video URL
				if ($videoData!==false) {
					$video_type  = $videoData['video_type'];
					$video_id    = $videoData['video_id'];
					if($video_type == "vimeo") {
						$videoRel = "shadowbox;height=".$popupHeight.";width=".$popupWidth.";";
						$videoURL = 'http://vimeo.com/moogaloop.swf?clip_id='.$video_id.'&autoplay='.$autoPlay.'';
					} elseif($video_type == "youtube") {
						$videoRel = 'shadowbox;height='.$popupHeight.';width='.$popupWidth.';options={flashVars:{autostart: '.$autoPlay .'}}';
						$videoURL = "http://www.youtube.com/v/".$video_id;
					} 
				} else {
					$videoURL = $object["videourl"];
					$videoRel = 'shadowbox;height='.$popupHeight.';width='.$popupWidth.';options={flashVars:{skin: '.Mage::getBaseUrl('js').'mediaappearance/skin01.zip,autostart: '.$autoPlay.'}}';
				}
				
			} 
			
			if($object->getId()) {
				$videoLink = '&nbsp;&nbsp;<a href="'.$videoURL.'" rel="'.$videoRel.'" title="'.$object["title"].'" >View current file</a>';
				$imgSrc = '<img src="'.$imgURL.'" width="100" height="73" border="0"/>';  
			} else {
				$videoLink = "";
				$imgSrc = "";
			}
			
			
			
		} catch (Exception $e) {
			$videoLink = "";
		}
		$html  = "";
		$html  =	'<tr>';
		$html  .=	'<td class="label"><label for="mediatype">Choose Video Type</label></td>';
		$html  .=	'<td class="value">';
		$html  .=	'<input type="radio" '.$_Typevalfile.' onclick="checkRadios();" id="video_typefile" value="1" name="mediatype"><label for="video_typefile" class="inline">&nbsp;Media File</label>&nbsp;';
		$html  .=	'<input type="radio" id="video_typeurl" '.$_Typevalurl.' onclick="checkRadios();" value="2" name="mediatype"><label for="video_typeurl" class="inline">&nbsp;URL</label>&nbsp;';
		$html  .=	'<p class="nm"><small>(If you want to upload file select (<b>Media File</b>) if you want to put yourtube video or link of video select second option)</small></p>            </td>';
		$html  .=	'</tr>';
		
		$html  .=	'<tr id="video_file_block">';
		$html .=	'<td class="label"><label for="my_file_uploader">Video File</label></td>';
		$html .=	'<td class="value">';
		$html .=	'<input type="file" value="" name="my_file_uploader" id="my_file_uploader">'.$videoLink;
		$html .=	'<p class="nm"><small>(Supported Format FLV, MPEG, MP4, JPEG, PNG, GIF, SWF)</small></p></td>';
		$html .=	'</tr>';
		
		$html .= 	'<tr id="video_url_block" style="display:none">';
		$html .= 	'<td class="label"><label for="videourl">Video URL</label></td>';
		$html .= 	'<td class="value">';
		$html .=  	'<input type="text" class=" input-text" value="'.$_val["videourl"].'" name="videourl" id="videourl">'.$videoLink;
		$html .= 	'<p class="nm"><small>(In URL field out youtube or Vimeo URL OR complete path of video e.g http://www.domain.com/media/abc.flv)</small></p></td>';
		$html .= 	'</tr>';
		
		$html .= 	'<tr>';
		$html .= 	'<td class="label"><label for="my_thumb_uploader">Media Thumb</label></td>';
		$html .= 	'<td class="value">';
		if(!isset($imgSrc)) {
			$imgSrc = "";
		}
		$html .=  	'<input type="file" style="float:left;" value="" name="my_thumb_uploader" id="my_thumb_uploader">'.$imgSrc;
		$html .= 	'</td>';
		$html .= 	'</tr>';
		
		$html .= '<script type="text/javascript">';
		$html .= "var checkRadios = function(){
					if ($('video_typefile').checked){
						
						$('video_file_block').show();
						$('video_url_block').hide();
			
					} else if($('video_typeurl').checked) {
			
						$('video_url_block').show();
						$('video_file_block').hide();
					}
				}
				window.onload = function() {
					checkRadios();
				}";
				
		$html .= '
			Shadowbox.init({
				overlayOpacity: 0.8,
				flashVars: {
					skin: "'.Mage::getBaseUrl('js')."mediaappearance/skin01.zip".'"
				}
			});';
		
		$html .= '</script>';

        return $html;
    }

}
