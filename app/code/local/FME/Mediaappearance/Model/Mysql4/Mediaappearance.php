<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/

class FME_Mediaappearance_Model_Mysql4_Mediaappearance extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the mediaappearance_id refers to the key field in your database table.
        $this->_init('mediaappearance/mediaappearance', 'mediaappearance_id');
    }
	
	 protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
    	
        $select = $this->_getReadAdapter()->select()
            ->from($this->getTable('media_store'))
            ->where('mediaappearance_id = ?', $object->getId());

        if ($data = $this->_getReadAdapter()->fetchAll($select)) {
            $storesArray = array();
            foreach ($data as $row) {
                $storesArray[] = $row['store_id'];
            }
            $object->setData('store_id', $storesArray);
        }

        return parent::_afterLoad($object);
        
    }
	
	/**
     * Process page data before saving
     *
     * @param Mage_Core_Model_Abstract $object
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
			
        $condition = $this->_getWriteAdapter()->quoteInto('mediaappearance_id = ?', $object->getId());
        $this->_getWriteAdapter()->delete($this->getTable('media_store'), $condition);
		$this->_getWriteAdapter()->delete($this->getTable('media_category_video'), $condition);
    
	
		//Get All Selected Categories
		$string = $_POST['category_ids'];
		$string = trim($string, ',');
						
		$catIds = explode(",", $string);	
		$result = array_unique($catIds);
		
		 foreach ($result as $category) {
            $categoryArray = array();
            $categoryArray['mediaappearance_id'] = $object->getId();
            $categoryArray['category_id'] = $category;
            $this->_getWriteAdapter()->insert($this->getTable('media_category_video'), $categoryArray);
        }
	
        foreach ((array)$object->getData('stores') as $store) {
            $storeArray = array();
            $storeArray['mediaappearance_id'] = $object->getId();
            $storeArray['store_id'] = $store;
            $this->_getWriteAdapter()->insert($this->getTable('media_store'), $storeArray);
        }
	
        return parent::_afterSave($object);
        
    }
	
}