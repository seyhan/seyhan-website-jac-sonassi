<?php
/**
 * Faqs extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php

 * @category   FME
 * @package    Faqs
 * @author     Kamran Rafiq Malik <kamran.malik@unitedsol.net>
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 */

class FME_Mediaappearance_Model_Mysql4_Videoblocks_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('mediaappearance/videoblocks');
    }
    
    public function addAttributeToFilter($attribute, $condition=null, $joinType='inner')
    {
        switch( $attribute ) {
            case 'status':
                $conditionSql = $this->_getConditionSql($attribute, $condition);
                $this->getSelect()->where($conditionSql);
                return $this;
                break;
            default:
                parent::addAttributeToFilter($attribute, $condition, $joinType);
        }
        return $this;
    }
	
	public function addBlockIdFilter($id = 0)
    {
        $this->getSelect()
            ->where('related.media_block_id=?', (int)$id);

        return $this;
    }
	
	public function addBlockIdentiferFilter($identifier = '')
    {
        $this->getSelect()
            ->where('main_table.block_identifier=?', $identifier);

        return $this;
    }
	
	
}