<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php 
 *
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/

class FME_Mediaappearance_Model_Videoblocks extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('mediaappearance/videoblocks');
    }
	
	/**
     * Retrieve related articles
     *
     * @return array
     */
    public function getRelatedMedia($blockId)
    {
			$mediavideoblockTable = Mage::getSingleton('core/resource')->getTableName('media_block_videos');
			$mediaTable = Mage::getSingleton('core/resource')->getTableName('mediaappearance');
			
			$collection = Mage::getModel('mediaappearance/videoblocks')->getCollection()
					  ->addBlockIdFilter($blockId);
					  
			$collection->getSelect()
            ->joinLeft(array('related' => $mediavideoblockTable),
                        'main_table.media_block_id = related.media_block_id'
                )
			 ->joinLeft(array('media' => $mediaTable),
                        'related.mediaappearance_id = media.mediaappearance_id'
                )
			->order('media.mediaappearance_id');
			
			return $collection->getData();

    }
	
	public function checkIdentifier($identifier, $storeId)
    {
        return $this->_getResource()->checkIdentifier($identifier, $storeId);
    }
}