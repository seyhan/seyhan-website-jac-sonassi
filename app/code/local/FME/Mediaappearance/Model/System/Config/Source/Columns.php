<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/
 
class FME_Mediaappearance_Model_System_Config_Source_Columns
{

    public function toOptionArray()
    {
        return array(
            array('value'=>1, 'label'=>Mage::helper('adminhtml')->__('Yes, all pages')),
			array('value'=>2, 'label'=>Mage::helper('adminhtml')->__('Yes, only media page')),
            array('value'=>0, 'label'=>Mage::helper('adminhtml')->__('No')),
        );
    }

}
