<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/
 
class FME_Mediaappearance_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    public function initControllerRouters($observer)
    {    		
        $front = $observer->getEvent()->getFront();
        $router = new FME_Mediaappearance_Controller_Router();
        $front->addRouter('mediaappearance', $router);
        
    }

    public function match(Zend_Controller_Request_Http $request)
    {
		
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }
		
        
        $route = Mage::helper('mediaappearance')->getListIdentifier();		
       	$identifier = trim($request->getPathInfo(), '/');		
        $identifier = str_replace(Mage::helper('mediaappearance')->getSeoUrlSuffix(), '', $identifier);
		            
        if ( $identifier == $route ) {
        	$request->setModuleName('mediaappearance')
        			->setControllerName('index')
        			->setActionName('index');
        			
        	return true;
        			
        }  
       
        return false;

    }
}