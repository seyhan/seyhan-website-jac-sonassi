<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/
 
class FME_Mediaappearance_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {	
	
		$collection = Mage::getModel('mediaappearance/mediaappearance')->getCollection()
							->addFieldToFilter('main_table.status', 1)
							->addOrder('main_table.created_time', 'desc')
							->getData();
							
		$itemsPerPage = Mage::helper('mediaappearance')->getListItemsPerPage();
		
		// Use paginator
		if ( $itemsPerPage != 0 ) {		
			$paginator = Zend_Paginator::factory((array)$collection);
			$paginator->setCurrentPageNumber((int)$this->_request->getParam('page', 1))
					  ->setItemCountPerPage($itemsPerPage);
			Mage::register('items', $paginator);
		} else {
			Mage::register('items', $collection);
		}
		
		$this->loadLayout(); 
		$this->getLayout()->getBlock('root')->setTemplate(Mage::getStoreConfig('mediaappearance/general/layout'));
		$this->renderLayout();
    }
}