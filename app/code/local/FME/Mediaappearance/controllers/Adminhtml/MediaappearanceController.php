<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE 
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/

class FME_Mediaappearance_Adminhtml_MediaappearanceController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('mediaappearance/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Media Manager'), Mage::helper('adminhtml')->__('Media Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	
	public function categoriesAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('mediaappearance/adminhtml_mediaappearance_edit_tab_categories')->toHtml()
        );   
    }
	
	public function categoriesJsonAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('mediaappearance/adminhtml_mediaappearance_edit_tab_categories')
                ->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }

	public function editAction() {
				
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('mediaappearance/mediaappearance')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('mediaappearance_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('mediaappearance/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Media Manager'), Mage::helper('adminhtml')->__('Media Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Media News'), Mage::helper('adminhtml')->__('Media News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('mediaappearance/adminhtml_mediaappearance_edit'))
				->_addLeft($this->getLayout()->createBlock('mediaappearance/adminhtml_mediaappearance_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mediaappearance')->__('Media does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		
		if ($data = $this->getRequest()->getPost()) {
			
			
			//Upload Video and Thumbnail  
			$files = $this->uploadFiles( $_FILES ); 
            if( $files && is_array($files) ){
                for( $f=0; $f<count($files); $f++ ){
                    if( $files[$f] ){
                        $fieldname = str_replace('_uploader','',$files[$f]['fieldname']);
                        if( array_key_exists($fieldname, $data) ){
							if($fieldname == "my_file") {
                            	$data['filename'] = $files[$f]['url'];
							}
							if($fieldname == "my_thumb") {
								$data['filethumb'] = $files[$f]['url'];
							}
                        }     
                    }     
                }   
            } 
			 			 			
			//Save Products ID"s that is realted to this video!
			$data['product_ids'] = $_POST['product_ids']; 
			
			//Save related category id's
			$categoryidsString  = trim($_POST['category_ids'], ',');
			$catIds = explode(",", $categoryidsString);		
			$result = array_unique($catIds);
			$comma_separated = implode(",", $result);
			$data['category_ids'] = $comma_separated;
						
			$model = Mage::getModel('mediaappearance/mediaappearance');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('mediaappearance')->__('Media was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mediaappearance')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('mediaappearance/mediaappearance');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $mediaappearanceIds = $this->getRequest()->getParam('mediaappearance');
        if(!is_array($mediaappearanceIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($mediaappearanceIds as $mediaappearanceId) {
                    $mediaappearance = Mage::getModel('mediaappearance/mediaappearance')->load($mediaappearanceId);
                    $mediaappearance->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($mediaappearanceIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $mediaappearanceIds = $this->getRequest()->getParam('mediaappearance');
        if(!is_array($mediaappearanceIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select Media(s)'));
        } else {
            try {
                foreach ($mediaappearanceIds as $mediaappearanceId) {
                    $mediaappearance = Mage::getSingleton('mediaappearance/mediaappearance')
                        ->load($mediaappearanceId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($mediaappearanceIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'mediaappearance.csv';
        $content    = $this->getLayout()->createBlock('mediaappearance/adminhtml_mediaappearance_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'mediaappearance.xml';
        $content    = $this->getLayout()->createBlock('mediaappearance/adminhtml_mediaappearance_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
	
	public function toOptionArray($isMultiselect) {
		 
        if (!$this->_options) {
            $this->_options = Mage::getResourceModel('core/language_collection')->loadData()->toOptionArray();
        }
        $options = $this->_options;
        if(!$isMultiselect){
            array_unshift($options, array('value'=>'', 'label'=>''));
        }
        return $options;
    }
	
	public function productAction() {
		
        $this->_initProduct();
        $this->loadLayout();
		$data=$this->getRequest()->getPost();
        $this->renderLayout();
    }
	
	public function productGridAction() {
		
			echo 'Function ===> productgridaction';
			$this->_initProduct();
			$this->loadLayout();
			$data=$this->getRequest()->getPost();
			$this->renderLayout();
	}
	
	public function gridAction() {
	 
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('mediaappearance/adminhtml_mediaappearance_edit_tab_product')->toHtml()
		);
	
	}
	
    public function gridOnlyAction()
    {
        echo 'Function ===> GridOnlyAction';
		$this->_initProduct();
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('adminhtml/mediaappearance_edit_tab_product')
                ->toHtml()
        );
    }
	
	protected function uploadFiles( $files ){
        if( !empty($files) && is_array($files) ){
            $result = array();
            foreach( $files as $file=>$info ){
                $result[] = $this->uploadFile( $file );
            }
            return $result;
        }
    }
	
	protected function uploadFile( $file_name ){

        if( !empty($_FILES[$file_name]['name']) ){
            $result = array();
            $dynamicScmsURL = 'mediaappearance' . DS . 'files';
            $baseScmsMediaURL = Mage::getBaseUrl('media') . DS . 'mediaappearance' . DS . 'files';
            $baseScmsMediaPath = Mage::getBaseDir('media') . DS .  'mediaappearance' . DS . 'files';
            
            $uploader = new Varien_File_Uploader( $file_name );
            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','flv','mp4','mp3','avi'));
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $result = $uploader->save( $baseScmsMediaPath );
       
            $file = str_replace(DS, '/', $result['file']);
            if( substr($baseScmsMediaURL, strlen($baseScmsMediaURL)-1)=='/' && substr($file, 0, 1)=='/' )    $file = substr($file, 1);
						
            $ScmsMediaUrl = $dynamicScmsURL.$file;
            
            $result['fieldname'] = $file_name;
            $result['url'] = $ScmsMediaUrl;
            $result['file'] = $result['file'];
            return $result;
        } else {
            return false;
        }
    } 
}