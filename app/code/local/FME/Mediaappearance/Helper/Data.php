<?php
/**
 * Media Gallery & Product Videos extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    Mediaappearance
 * @copyright  Copyright 2010 � free-magentoextensions.com All right reserved
 **/

class FME_Mediaappearance_Helper_Data extends Mage_Core_Helper_Abstract
{
	
	const XML_PATH_LIST_PAGE_TITLE				=	'mediaappearance/list/page_title';
	const XML_PATH_LIST_IDENTIFIER				=	'mediaappearance/list/identifier';
	const XML_PATH_LIST_ITEMS_PER_PAGE			=	'mediaappearance/list/items_per_page'; 
	const XML_PATH_LIST_LIMIT_DESCRIPTION			=	'mediaappearance/list/limit_description';
	const XML_PATH_LIST_META_DESCRIPTION			=	'mediaappearance/list/meta_description';
	const XML_PATH_LIST_META_KEYWORDS			=	'mediaappearance/list/meta_keywords';
	const XML_PATH_SEO_URL_SUFFIX				=	'mediaappearance/seo/url_suffix'; 
	const XML_PATH_FEATUREDBLOCK_LEFT			= 	'mediaappearance/featuredblock/left';
	const XML_PATH_FEATUREDBLOCK_RIGHT			= 	'mediaappearance/featuredblock/right';
	const XML_PATH_FEATUREDBLOCK_TITLE			= 	'mediaappearance/featuredblock/title'; 
	const XML_PATH_VIDEOSBLOCK_LEFT				= 	'mediaappearance/videosblock/left';
	const XML_PATH_VIDEOSBLOCK_RIGHT			= 	'mediaappearance/videosblock/right';
	const XML_PATH_VIDEOSBLOCK_TITLE			= 	'mediaappearance/videosblock/title';
	
	
	public function getListPageTitle()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_PAGE_TITLE);
	}
	
	public function getFeaturedVideosBlockTitle()
	{
		return Mage::getStoreConfig(self::XML_PATH_FEATUREDBLOCK_TITLE);
	}
	
	public function getVideosBlockTitle()
	{
		return Mage::getStoreConfig(self::XML_PATH_VIDEOSBLOCK_TITLE);
	}
	
	public function getListIdentifier()
	{
		$identifier = Mage::getStoreConfig(self::XML_PATH_LIST_IDENTIFIER);
		if ( !$identifier ) {
			$identifier = 'mediaappearance';
		}
		return $identifier;
	}
	
	public function geturlIdentifier()
	{
		$identifier = $this->getListIdentifier() . Mage::getStoreConfig(self::XML_PATH_SEO_URL_SUFFIX);
		return $identifier;
	}
	
	public function getListItemsPerPage()
	{
		return (int)Mage::getStoreConfig(self::XML_PATH_LIST_ITEMS_PER_PAGE);
	}
	
	public function getListLimitDescription()
	{
		return (int)Mage::getStoreConfig(self::XML_PATH_LIST_LIMIT_DESCRIPTION);
	}
	
	public function getListMetaDescription()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_META_DESCRIPTION);
	}
	
	public function getListMetaKeywords()
	{
		return Mage::getStoreConfig(self::XML_PATH_LIST_META_KEYWORDS);
	}
	
	public function getUrl($identifier = null)
	{
		
		if ( is_null($identifier) ) {
			$url = Mage::getUrl('') . self::getListIdentifier() . self::getSeoUrlSuffix();
		} else {
			$url = Mage::getUrl('') . $identifier . self::getSeoUrlSuffix();
		}

		return $url;
		
	}
	
    public function getSeoUrlSuffix()
	{
		return Mage::getStoreConfig(self::XML_PATH_SEO_URL_SUFFIX);
	}
    
    public function video_thumbnail($url) {
		$youtube = array();
		if( !strpos($url, "#!v=") === false ){  //Em caso de ser um link de quando clica nos related
			$url = str_replace('#!v=','?v=',$url);
		}
		parse_str( parse_url( $url, PHP_URL_QUERY ) );
		if( isset( $v ) ){
			$youtubeID = $v;
		} else { //Se n�o achou, � por que � o link de um video de canal ex: http://www.youtube.com/user/laryssap#p/a/u/1/SAXVMaLL94g
				 //If not found, is because is a link from a user channel ex: http://www.youtube.com/user/laryssap#p/a/u/1/SAXVMaLL94g
			$youtubeID = substr( $url, strrpos( $url,'/') + 1, 11);
		}
		
		$youtube[0] = $youtubeID;
		$youtube[1] ="http://img.youtube.com/vi/".$youtubeID."/0.jpg";
		$youtube[2] ="http://www.youtube.com/v/".$youtubeID;
		
		return $youtube;
	}
	
	public function resizeImage($imageName, $width=NULL, $height=NULL, $imagePath=NULL)
	{
		$imagePath = str_replace("/", DS, $imagePath);
		$imagePathFull = Mage::getBaseDir('media') . DS . $imagePath . DS . $imageName;
	
		if($width == NULL && $height == NULL) { 
			$width = 100;
			$height = 100;
		}
		$resizePath = $width . 'x' . $height;
		$resizePathFull = Mage::getBaseDir('media') . DS . $imagePath . DS . $resizePath . DS . $imageName;
	
		if (file_exists($imagePathFull) && !file_exists($resizePathFull)) {
			$imageObj = new Varien_Image($imagePathFull);
			$imageObj->constrainOnly(TRUE);
			$imageObj->keepAspectRatio(false);
			$imageObj->resize($width,$height);
			$imageObj->save($resizePathFull);
		}
	
		$imagePath=str_replace(DS, "/", $imagePath);
		return Mage::getBaseUrl("media") . $imagePath . "/" . $resizePath . "/" . $imageName;
	}
	
	public function recursiveReplace($search, $replace, $subject)
    {
        if(!is_array($subject))
            return $subject;

        foreach($subject as $key => $value)
            if(is_string($value))
                $subject[$key] = str_replace($search, $replace, $value);
            elseif(is_array($value))
                $subject[$key] = self::recursiveReplace($search, $replace, $value);

        return $subject;
    }
	
	public function getImageUrl($image_file)
    {
        $url = false;
        $url = Mage::getBaseUrl('media'). $image_file;
        return $url;
    }
	
	 public function getFileExists($image_file)
    {
        $file_exists = false;
        $file_exists = file_exists(Mage::getBaseUrl('media'). $image_file);
        return $file_exists;
    }
	
	 public function isMageVerAtLeast($version_str) {
       $version_str_sections = explode('.', $version_str);
       $mage_version_sections = explode('.', Mage::getVersion());
       foreach( $version_str_sections as $key => $value){ 
       		if(!isset($mage_version_sections[$key])) break;
       		
            if ($mage_version_sections[$key] > $value ){
              return true;
            }
            if ($mage_version_sections[$key] < $value ) {
              return false;
            }
       }
       return true;    
    }

	public function video_info($url) {
	
		// Handle Youtube
		if (strpos($url, "youtube.com")) {
			$url = parse_url($url);
			$vid = parse_str($url['query'], $output);
			if(!isset($output['v'])) {
				$video_id = '0';
			} else {
				$video_id = $output['v'];
			}
			$data['video_type'] = 'youtube';
			$data['video_id'] = $video_id;
			$xml = simplexml_load_file("http://gdata.youtube.com/feeds/api/videos?q=$video_id");
		
			foreach ($xml->entry as $entry) {
				// get nodes in media: namespace
				$media = $entry->children('http://search.yahoo.com/mrss/');
				
				// get video player URL
				$attrs = $media->group->player->attributes();
				$watch = $attrs['url']; 
				
				// get video thumbnail
				$data['thumb_1'] = $media->group->thumbnail[0]->attributes(); // Thumbnail 1
				$data['thumb_2'] = $media->group->thumbnail[1]->attributes(); // Thumbnail 2
				$data['thumb_3'] = $media->group->thumbnail[2]->attributes(); // Thumbnail 3
				$data['thumb_large'] = $media->group->thumbnail[3]->attributes(); // Large thumbnail
				$data['tags'] = $media->group->keywords; // Video Tags
				$data['cat'] = $media->group->category; // Video category
				$attrs = $media->group->thumbnail[0]->attributes();
				$thumbnail = $attrs['url']; 
				
				// get <yt:duration> node for video length
				$yt = $media->children('http://gdata.youtube.com/schemas/2007');
				$attrs = $yt->duration->attributes();
				$data['duration'] = $attrs['seconds'];
				
				// get <yt:stats> node for viewer statistics
				$yt = $entry->children('http://gdata.youtube.com/schemas/2007');
				$attrs = $yt->statistics->attributes();
				$data['views'] = $viewCount = $attrs['viewCount']; 
				$data['title']=$entry->title;
				$data['info']=$entry->content;
				
				// get <gd:rating> node for video ratings
				$gd = $entry->children('http://schemas.google.com/g/2005'); 
				if ($gd->rating) {
					$attrs = $gd->rating->attributes();
					$data['rating'] = $attrs['average']; 
				} else { $data['rating'] = 0;}
			} // End foreach
		} // End Youtube
		
		// Handle Vimeo
		else if (strpos($url, "vimeo.com")) {
			$video_id=explode('vimeo.com/', $url);
			$video_id=$video_id[1];
			$data['video_type'] = 'vimeo';
			$data['video_id'] = $video_id;
			$xml = simplexml_load_file("http://vimeo.com/api/v2/video/$video_id.xml");
				
			foreach ($xml->video as $video) {
				$data['id']=$video->id;
				$data['title']=$video->title;
				$data['info']=$video->description;
				$data['url']=$video->url;
				$data['upload_date']=$video->upload_date;
				$data['mobile_url']=$video->mobile_url;
				$data['thumb_small']=$video->thumbnail_small;
				$data['thumb_medium']=$video->thumbnail_medium;
				$data['thumb_large']=$video->thumbnail_large;
				$data['user_name']=$video->user_name;
				$data['urer_url']=$video->urer_url;
				$data['user_thumb_small']=$video->user_portrait_small;
				$data['user_thumb_medium']=$video->user_portrait_medium;
				$data['user_thumb_large']=$video->user_portrait_large;
				$data['user_thumb_huge']=$video->user_portrait_huge;
				$data['likes']=$video->stats_number_of_likes;
				$data['views']=$video->stats_number_of_plays;
				$data['comments']=$video->stats_number_of_comments;
				$data['duration']=$video->duration;
				$data['width']=$video->width;
				$data['height']=$video->height;
				$data['tags']=$video->tags;
			} // End foreach
		} // End Vimeo
		
		// Set false if invalid URL
		else { $data = false; }
		
		return $data;
	
	}


	
}