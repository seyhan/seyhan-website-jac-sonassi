<?php

$installer = $this;
$installer->startSetup();
/*
$dataflowData = array(
    array(
        'name'         => 'Import Products (CSV)',
        'actions_xml'  => '<action type="dataflow/convert_parser_csv" method="parse">'."\r\n".'    <var name="delimiter"><![CDATA[,]]></var>'."\r\n".'    <var name="enclose"><![CDATA["]]></var>'."\r\n".'    <var name="fieldnames">true</var>'."\r\n".'    <var name="store"><![CDATA[0]]></var>'."\r\n".'    <var name="number_of_records">1</var>'."\r\n".'    <var name="root_catalog_id"><![CDATA[2]]></var>'."\r\n".'    <var name="reimport_images"><![CDATA[true]]></var>'."\r\n".'    <var name="deleteall_andreimport_images"><![CDATA[true]]></var>'."\r\n".'    <var name="exclude_images"><![CDATA[false]]></var>'."\r\n".'    <var name="exclude_gallery_images"><![CDATA[false]]></var>'."\r\n".'    <var name="append_tier_prices"><![CDATA[true]]></var>'."\r\n".'    <var name="append_group_prices"><![CDATA[false]]></var>'."\r\n".'    <var name="append_categories"><![CDATA[false]]></var>'."\r\n".'    <var name="decimal_separator"><![CDATA[.]]></var>'."\r\n".'    <var name="adapter">productimportexport/convert_adapter_productimport</var>'."\r\n".'    <var name="method">parse</var>'."\r\n".'</action>',
        'gui_data'     => null,
        'direction'    => 'import',
        'entity_type'  => null,
        'store_id'     => 0,
        'data_transfer'=> 'interactive',
        'is_commerce_extensions' => 1
    ),
    array(
        'name'         => 'Import Products (XML)',
        'actions_xml'  => '<action type="dataflow/convert_parser_xml_excel" method="parse">'."\r\n".'    <var name="single_sheet"><![CDATA[]]></var>'."\r\n".'    <var name="fieldnames">true</var>'."\r\n".'    <var name="store"><![CDATA[0]]></var>'."\r\n".'    <var name="number_of_records">1</var>'."\r\n".'    <var name="root_catalog_id"><![CDATA[2]]></var>'."\r\n".'    <var name="reimport_images"><![CDATA[true]]></var>'."\r\n".'    <var name="deleteall_andreimport_images"><![CDATA[true]]></var>'."\r\n".'    <var name="exclude_images"><![CDATA[false]]></var>'."\r\n".'    <var name="exclude_gallery_images"><![CDATA[false]]></var>'."\r\n".'    <var name="append_tier_prices"><![CDATA[true]]></var>'."\r\n".'    <var name="append_group_prices"><![CDATA[false]]></var>'."\r\n".'    <var name="decimal_separator"><![CDATA[.]]></var>'."\r\n".'    <var name="adapter">productimportexport/convert_adapter_productimport</var>'."\r\n".'    <var name="method">parse</var>'."\r\n".'</action>',
        'gui_data'     => null,
        'direction'    => 'import',
        'entity_type'  => null,
        'store_id'     => 0,
        'data_transfer'=> 'interactive',
        'is_commerce_extensions' => 1
    ),
    array(
        'name'         => 'Import Products (CSV) Multistore',
        'actions_xml'  => '<action type="dataflow/convert_parser_csv" method="parse">'."\r\n".'    <var name="delimiter"><![CDATA[,]]></var>'."\r\n".'    <var name="enclose"><![CDATA["]]></var>'."\r\n".'    <var name="fieldnames">true</var>'."\r\n".'    <var name="number_of_records">1</var>'."\r\n".'    <var name="root_catalog_id"><![CDATA[2]]></var>'."\r\n".'    <var name="reimport_images"><![CDATA[false]]></var>'."\r\n".'    <var name="exclude_images"><![CDATA[true]]></var>'."\r\n".'    <var name="exclude_gallery_images"><![CDATA[true]]></var>'."\r\n".'    <var name="decimal_separator"><![CDATA[.]]></var>'."\r\n".'    <var name="adapter">productimportexport/convert_adapter_productimport</var>'."\r\n".'    <var name="method">parse</var>'."\r\n".'</action>',
        'gui_data'     => null,
        'direction'    => 'import',
        'entity_type'  => null,
        'store_id'     => 0,
        'data_transfer'=> 'interactive',
        'is_commerce_extensions' => 1
    ),
    array(
        'name'         => 'Export Products (CSV)',
        'actions_xml'  => '<action type="productimportexport/convert_adapter_productimport" method="load">'."\r\n".'    <var name="store"><![CDATA[0]]></var>'."\r\n".'    <var name="filter/adressType"><![CDATA[default_billing]]></var>'."\r\n".'</action>'."\r\n".''."\r\n".'<action type="productimportexport/convert_parser_productexport" method="unparse">'."\r\n".'    <var name="store"><![CDATA[0]]></var>'."\r\n".'    <var name="recordlimitstart"><![CDATA[0]]></var>'."\r\n".'    <var name="recordlimitend"><![CDATA[100]]></var>'."\r\n".'    <var name="export_grouped_position"><![CDATA[false]]></var>'."\r\n".'    <var name="export_related_position"><![CDATA[false]]></var>'."\r\n".'    <var name="export_crossell_position"><![CDATA[false]]></var>'."\r\n".'    <var name="export_upsell_position"><![CDATA[false]]></var>'."\r\n".'    <var name="export_category_paths"><![CDATA[false]]></var>'."\r\n".'</action>'."\r\n".''."\r\n".'<action type="dataflow/convert_mapper_column" method="map"></action>'."\r\n".''."\r\n".'<action type="dataflow/convert_parser_csv" method="unparse">'."\r\n".'    <var name="delimiter"><![CDATA[,]]></var>'."\r\n".'    <var name="enclose"><![CDATA["]]></var>'."\r\n".'    <var name="fieldnames">true</var>'."\r\n".'    </action>'."\r\n".''."\r\n".'<action type="dataflow/convert_adapter_io" method="save">'."\r\n".'    <var name="type">file</var>'."\r\n".'    <var name="path">var/export</var>'."\r\n".'    <var name="filename"><![CDATA[export_products.csv]]></var>'."\r\n".'</action>',
        'gui_data'     => null,
        'direction'    => 'export',
        'entity_type'  => null,
        'store_id'     => 0,
        'data_transfer'=> 'file',
        'is_commerce_extensions' => 1
    ),
    array(
        'name'         => 'Export Products (XML)',
        'actions_xml'  => '<action type="productimportexport/convert_adapter_productimport" method="load">'."\r\n".'    <var name="store"><![CDATA[0]]></var>'."\r\n".'    <var name="filter/adressType"><![CDATA[default_billing]]></var>'."\r\n".'</action>'."\r\n".''."\r\n".'<action type="productimportexport/convert_parser_productexport" method="unparse">'."\r\n".'    <var name="store"><![CDATA[0]]></var>'."\r\n".'    <var name="recordlimitstart"><![CDATA[0]]></var>'."\r\n".'    <var name="recordlimitend"><![CDATA[100]]></var>'."\r\n".'    <var name="export_grouped_position"><![CDATA[false]]></var>'."\r\n".'    <var name="export_related_position"><![CDATA[false]]></var>'."\r\n".'    <var name="export_crossell_position"><![CDATA[false]]></var>'."\r\n".'    <var name="export_upsell_position"><![CDATA[false]]></var>'."\r\n".'    <var name="export_category_paths"><![CDATA[false]]></var>'."\r\n".'</action>'."\r\n".''."\r\n".'<action type="dataflow/convert_mapper_column" method="map"></action>'."\r\n".''."\r\n".'<action type="dataflow/convert_parser_xml_excel" method="unparse">'."\r\n".'    <var name="single_sheet"><![CDATA[]]></var>'."\r\n".'    <var name="fieldnames">true</var>'."\r\n".'</action>'."\r\n".''."\r\n".'<action type="dataflow/convert_adapter_io" method="save">'."\r\n".'    <var name="type">file</var>'."\r\n".'    <var name="path">var/export</var>'."\r\n".'    <var name="filename"><![CDATA[export_products.xml]]></var>'."\r\n".'</action>',
        'gui_data'     => null,
        'direction'    => 'export',
        'entity_type'  => null,
        'store_id'     => 0,
        'data_transfer'=> 'file',
        'is_commerce_extensions' => 1
    )
);

foreach ($dataflowData as $profileData)
{
    Mage::getModel('dataflow/profile')->setData($profileData)->save();
}
*/
$installer->endSetup();