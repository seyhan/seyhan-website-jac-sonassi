<?php
/**
 * Productimpotskucategoryids.php
 * CommerceThemes @ InterSEC Solutions LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.commerceextensions.com/LICENSE-M1.txt
 *
 * @category   Product
 * @package    Productimpotskucategoryids
 * @copyright  Copyright (c) 2003-2009 CommerceExtensions @ InterSEC Solutions LLC. (http://www.commerceextensions.com)
 * @license    http://www.commerceextensions.com/LICENSE-M1.txt
 */ 

class CommerceExtensions_Productimportexport_Model_Convert_Adapter_Productimportskucategoryids
extends Mage_Catalog_Model_Convert_Adapter_Product
{
	
	/**
	* Save product (import)
	* 
	* @param array $importData 
	* @throws Mage_Core_Exception
	* @return bool 
	*/
	public function saveRow( array $importData )
	{
		#$product = $this -> getProductModel();
		$product = $this->getProductModel()->reset();

		$productIDcheckifnew = $product->getIdBySku(trim($importData['sku']));
		if ($productIDcheckifnew) {
			
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix');
			$sql = "select * from ".$prefix."catalog_product_entity where sku='".strval(trim($importData['sku']))."'";
			
			$rs = $write->fetchAll($sql);
			if($rs)
			{
				//UPDATE FOR CATEGORY IDS
				if(isset($importData['category_ids']))
				{
					$categoryIds = $importData['category_ids'];
					if($this->getBatchParams('append_categories') == "true") { 
						$productModel = Mage::getModel('catalog/product')->load($rs[0]['entity_id']);
						$cats = $productModel->getCategoryIds();
						$catsarray = explode(",",$categoryIds);
						$finalcatsimport = array_merge($cats, $catsarray);
						$productModel->setCategoryIds($finalcatsimport);
						$productModel->save();
					} else {
						$productModel = Mage::getModel('catalog/product')->load($rs[0]['entity_id']);
						$productModel->setCategoryIds($categoryIds);
						$productModel->save();
					}
				}
			} //if $rs is true
				
		}
		return true;
	} 
}