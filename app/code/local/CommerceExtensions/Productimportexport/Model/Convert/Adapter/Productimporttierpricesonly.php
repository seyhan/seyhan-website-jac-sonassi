<?php
/**
 * Product_import.php
 * CommerceThemes @ InterSEC Solutions LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.commercethemes.com/LICENSE-M1.txt
 *
 * @category   Product
 * @package    Productimport
 * @copyright  Copyright (c) 2003-2009 CommerceThemes @ InterSEC Solutions LLC. (http://www.commercethemes.com)
 * @license    http://www.commercethemes.com/LICENSE-M1.txt
 */ 

class CommerceExtensions_Productimportexport_Model_Convert_Adapter_Productimporttierpricesonly
extends Mage_Catalog_Model_Convert_Adapter_Product
{
	
	/**
	* Save product (import)
	* 
	* @param array $importData 
	* @throws Mage_Core_Exception
	* @return bool 
	*/
	public function saveRow( array $importData )
	{
		#$product = $this -> getProductModel();
		$product = $this->getProductModel()
            ->reset();
		#$product -> setData( array() );

		$productIDcheckifnew = $product->getIdBySku($importData['sku']);
		if ($productIDcheckifnew) {
			
			$read = Mage::getSingleton('core/resource')->getConnection('core_read');
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix');
			$sql = "select * from ".$prefix."catalog_product_entity where sku='".strval(trim($importData['sku']))."'";
			
			$rs = $write->fetchAll($sql);
			if($rs)
			{
				//UPDATE FOR PRICE
				if(isset($importData['price']))
				{
					$priceforupdate = $importData['price'];
					
					if(isset($importData['store_id'])) {
						$store_id = $importData['store_id'];
					} else {
						$store_id = '0';
					}
					$prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix');
					$read = Mage::getSingleton('core/resource')->getConnection('core_read');
					$write = Mage::getSingleton('core/resource')->getConnection('core_write');
					$entity_type_id = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
					
					$select_qry = "SELECT ".$prefix."catalog_product_entity.sku, ".$prefix."catalog_product_entity_decimal.value FROM ".$prefix."catalog_product_entity INNER JOIN ".$prefix."catalog_product_entity_decimal ON ".$prefix."catalog_product_entity_decimal.entity_id = ".$prefix."catalog_product_entity.entity_id WHERE ".$prefix."catalog_product_entity_decimal.store_id = '".$store_id."' AND ".$prefix."catalog_product_entity_decimal.attribute_id = (
						 SELECT attribute_id FROM ".$prefix."eav_attribute eav
						 WHERE eav.attribute_code = 'price' AND eav.entity_type_id = '".$entity_type_id."'
						) AND ".$prefix."catalog_product_entity.sku = '".strval(trim($importData['sku']))."'";
			 		
					$rs2 = $read->fetchAll($select_qry);
					
					if($rs2) {	
						#echo "WE HAVE TO UPDATE";
						$write = Mage::getSingleton('core/resource')->getConnection('core_write');
						$write->query("
						  UPDATE ".$prefix."catalog_product_entity_decimal val, ".$prefix."catalog_product_entity prod
						  SET  val.value = '".$priceforupdate."'
						  WHERE val.entity_id = prod.entity_id
					      AND val.store_id = '".$store_id."'
						  AND val.attribute_id = (
							 SELECT attribute_id FROM ".$prefix."eav_attribute eav
							 WHERE eav.entity_type_id = 4 
							   AND eav.attribute_code = 'price'
							)
						  AND prod.sku = '".strval(trim($importData['sku']))."'
						");
						
					} else {		
						#echo "WE HAVE TO INSERT";			
						$entity_type_id = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
						$select_qry =$read->query("SELECT attribute_id FROM `".$prefix."eav_attribute` WHERE `attribute_code`='price' and entity_type_id ='".$entity_type_id."'");
						$row = $select_qry->fetch();
						$attribute_id = $row['attribute_id'];
						
						$write->query("INSERT INTO ".$prefix."catalog_product_entity_decimal (entity_type_id,attribute_id,store_id,entity_id,value) VALUES ('".$entity_type_id."','".$attribute_id."','".$store_id."','".$productIDcheckifnew."','".$priceforupdate."')");
					}
				}
				//TIER PRICING
				if(isset($importData['tier_prices']))
				{
					
					if($this->getBatchParams('append_tier_prices') != "true") {
                         if(isset($importData['sku'])) {
                            $product = Mage::getModel('catalog/product');
                            $productId = $product->getIdBySku($importData['sku']);

                         }
						 $delete_qry =$read->query("DELETE FROM `".$prefix."catalog_product_entity_tier_price` WHERE `entity_id` = '".$productId."'");
						 #$deleted = $delete_qry->fetch();
					}
               		$incoming_tierps = explode('|',$importData['tier_prices']);
					$tps_toAdd = array();  
					
					foreach($incoming_tierps as $tier_str){
					
						 if (empty($tier_str)) continue;
						
						 $tmp = array();
						 $tmp = explode('=',$tier_str);
						
						 if(isset($importData['sku'])) {
						  $product = Mage::getModel('catalog/product');
						  $productId = $product->getIdBySku($importData['sku']);
						 }
						 $qty_format_number = number_format($tmp[1], 4, '.', '');
						 
						 if(isset($tmp[3])) {
						 	if($tmp[3] !="") {
								$website_id = $tmp[3];
							} else {
								$website_id = '0';
							}
						 }
						 if($tmp[0] == "32000") {
							 $select_qry =$read->query("select value_id from `".$prefix."catalog_product_entity_tier_price` where `entity_id` = $productId AND `qty` = '".$qty_format_number."' AND `all_groups` = 1 AND website_id = $website_id");
							 //echo "select value_id from `".$prefix."catalog_product_entity_tier_price` where `entity_id` = $productId AND `qty` = '".$qty_format_number."'  AND `customer_group_id` = $tmp[0]";
							 $newrow = $select_qry->fetch();
							 $newGroupId = $newrow['value_id'];
							 if($newGroupId != "") {
								//echo "ID: " . $newGroupId;
								$write = Mage::getSingleton('core/resource')->getConnection('core_write');
								$write->query("
								  UPDATE ".$prefix."catalog_product_entity_tier_price val
								  SET  val.value = $tmp[2], val.qty = $tmp[1]
								  WHERE val.value_id = '".$newGroupId."'
								");
							} else {
								//echo "ID2: " . $newGroupId;
								$write = Mage::getSingleton('core/resource')->getConnection('core_write');
								$write_qry =$write->query("Insert into `".$prefix."catalog_product_entity_tier_price` (entity_id,all_groups,customer_group_id,qty,value,website_id) VALUES ('$productId','1','0','".$tmp[1]."','".$tmp[2]."','".$website_id."')");
							
							}
						 
						 } else {
							 $select_qry =$read->query("select value_id from `".$prefix."catalog_product_entity_tier_price` where `entity_id` = $productId AND `qty` = '".$qty_format_number."' AND `customer_group_id` = $tmp[0] AND website_id = $website_id");
							 //echo "select value_id from `".$prefix."catalog_product_entity_tier_price` where `entity_id` = $productId AND `qty` = '".$qty_format_number."'  AND `customer_group_id` = $tmp[0]";
							 $newrow = $select_qry->fetch();
							 $newGroupId = $newrow['value_id'];
							 if($newGroupId != "") {
								//echo "ID: " . $newGroupId;
								$write = Mage::getSingleton('core/resource')->getConnection('core_write');
								$write->query("
								  UPDATE ".$prefix."catalog_product_entity_tier_price val
								  SET  val.value = $tmp[2], val.qty = $tmp[1], val.customer_group_id = $tmp[0]
								  WHERE val.value_id = '".$newGroupId."'
								");
							} else {
								//echo "ID2: " . $newGroupId;
								$write = Mage::getSingleton('core/resource')->getConnection('core_write');
								$write_qry =$write->query("Insert into `".$prefix."catalog_product_entity_tier_price` (entity_id,all_groups,customer_group_id,qty,value,website_id) VALUES ('$productId','0','".$tmp[0]."','".$tmp[1]."','".$tmp[2]."','".$website_id."')");
							
							}
						} //ends check on if customer is part of all groups
					}
				}
				
			} //if $rs is true
				
		} else {
		
		
		}// else if end for if new or existing
		
		return true;
	} 
	
	protected function userCSVDataAsArray( $data )
	{
		return explode( ',', str_replace( " ", "", $data ) );
	} 
	
	protected function skusToIds( $userData, $product )
	{
		$productIds = array();
		foreach ( $this -> userCSVDataAsArray( $userData ) as $oneSku ) {
			if ( ( $a_sku = ( int )$product -> getIdBySku( $oneSku ) ) > 0 ) {
				parse_str( "position=", $productIds[$a_sku] );
			} 
		} 
		return $productIds;
	} 
	
	protected function _removeFile( $file )
	{
		if ( file_exists( $file ) ) {
		$ext = substr(strrchr($file, '.'), 1);
			if( strlen( $ext ) == 4 ) {
				if ( unlink( $file ) ) {
					return true;
				} 
			}
		} 
		return false;
	} 
}