<?php
/**
 * Product_import.php
 * CommerceThemes @ InterSEC Solutions LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.commercethemes.com/LICENSE-M1.txt
 *
 * @category   Product
 * @package    Productimport
 * @copyright  Copyright (c) 2003-2009 CommerceThemes @ InterSEC Solutions LLC. (http://www.commercethemes.com)
 * @license    http://www.commercethemes.com/LICENSE-M1.txt
 */ 

class CommerceExtensions_Productimportexport_Model_Convert_Adapter_Productimportskuqty
extends Mage_Catalog_Model_Convert_Adapter_Product
{
	
	/**
	* Save product (import)
	* 
	* @param array $importData 
	* @throws Mage_Core_Exception
	* @return bool 
	*/
	public function saveRow( array $importData )
	{
		#$product = $this -> getProductModel();
		$product = $this->getProductModel()
            ->reset();
		#$product -> setData( array() );

		$productIDcheckifnew = $product->getIdBySku($importData['sku']);
		if ($productIDcheckifnew) {
			
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix');
			$sql = "select * from ".$prefix."catalog_product_entity where sku='".strval(trim($importData['sku']))."'";
			
			$rs = $write->fetchAll($sql);
			if($rs)
			{
				$sql2 = "select * from ".$prefix."cataloginventory_stock_item where product_id=" . $rs[0]['entity_id'];
				//echo $sql2;
				$rs2 = $write->fetchAll($sql2);
				if($rs2)
				{
					//run update code
					if(floatval($rs2[0]['qty']) == floatval(trim($importData['qty'])))
					{
						#echo "NO UPDATE NEEDED";
					}else{
						#echo "UPDATE REQUIRED";
						
						if(floatval(trim($importData['qty'])) > 0)
						{
							$instock = 1; //echo "IN STOCK";
						} else { 
							$instock = 0; //echo "OUT OF STOCK"; 
						}
						
						$write->update(
							''.$prefix.'cataloginventory_stock_item', 
							array(
								'qty' => number_format(trim($importData['qty']), 4),
								'is_in_stock' => intval($instock)
							)
							, '`item_id` = ' . $rs2[0]['item_id']);
						
						
						$sql3 = "select * from ".$prefix."cataloginventory_stock_status where product_id=" . $rs[0]['entity_id'];
						$rs3 = $write->fetchAll($sql3);
						if($rs3)
						{
							$write->update(
								''.$prefix.'cataloginventory_stock_status', 
								array(
									'qty' => number_format(trim($importData['qty']), 4),
									'stock_status' => intval($instock)
								)
								, '`product_id` = ' . $rs2[0]['item_id']);
						}
				 	} //if qty update is required or not
				} //if $rs2 is true
			} //if $rs is true
				
		} else {
		
		}// else if end for if new or existing
		return true;
	} 
	
	
	protected function userCSVDataAsArray( $data )
	{
		return explode( ',', str_replace( " ", "", $data ) );
	} 
	
	protected function _removeFile( $file )
	{
		if ( file_exists( $file ) ) {
		$ext = substr(strrchr($file, '.'), 1);
			if( strlen( $ext ) == 4 ) {
				if ( unlink( $file ) ) {
					return true;
				} 
			}
		} 
		return false;
	} 
}