<?php
/**
 * FreeShippingForAdmin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Company88
 * @package     Company88_FreeShippingForAdmin
 * @copyright   Copyright (c) 2012 Company88. KG (http://company88.nl)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
	  
class Company88_FreeshippingForAdmin_Model_Carrier_Freeshippingforadmin 
  extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
   /**
    * unique internal shipping method identifier
    *
    * @var string [a-z0-9_]
    */
    protected $_code = 'freeshippingforadmin';
 
    
	/**
     * Only a logged in admin user is allowed to use this
     * shipping option
     *
     * @return bool
     */
    protected function isAdminUser()
    {
   		/*check if we are logged in as administrator */
   		if (Mage::getSingleton('admin/session')->isLoggedIn())
   		{
   			return true;
   		}
    	return false; 
    }    
    
   /**
     * FreeShipping Rates Collector
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
		if (!$this->getConfigFlag('active') || !$this->isAdminUser())
		{
			return false;
		}

        $result = Mage::getModel('shipping/rate_result');
        $packageValue = $request->getPackageValue();

        $this->_updateFreeMethodQuote($request);

        $allow = ($request->getFreeShipping())
            || ($packageValue >= $this->getConfigData('free_shipping_subtotal'));

        if ($allow) {
            $method = Mage::getModel('shipping/rate_result_method');

            $method->setCarrier('freeshippingforadmin');
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod('freeshippingforadmin');
            $method->setMethodTitle($this->getConfigData('name'));

            $method->setPrice('0.00');
            $method->setCost('0.00');

            $result->append($method);
        }

        return $result;
    }

    /**
     * Allows free shipping when all product items have free shipping (promotions etc.)
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return void
     */
    protected function _updateFreeMethodQuote($request)
    {
        $freeShipping = false;
        $items = $request->getAllItems();
        $c = count($items);
        for ($i = 0; $i < $c; $i++) {
            if ($items[$i]->getProduct() instanceof Mage_Catalog_Model_Product) {
                if ($items[$i]->getFreeShipping()) {
                    $freeShipping = true;
                } else {
                    return;
                }
            }
        }
        if ($freeShipping) {
            $request->setFreeShipping(true);
        }
    }

  /**
   * This method is used when viewing / listing Shipping Methods with Codes programmatically
   */
  public function getAllowedMethods() {
    return array($this->_code => $this->getConfigData('name'));
  }
  
}