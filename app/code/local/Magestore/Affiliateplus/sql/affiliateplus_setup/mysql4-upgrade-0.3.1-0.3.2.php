<?php

$installer = $this;
$installer->startSetup();
$installer->run("
CREATE TABLE {$this->getTable('affiliateplus_quote_account')}(
  `quote_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `affiliate_account_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`quote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();