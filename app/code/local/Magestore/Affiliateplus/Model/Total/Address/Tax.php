<?php

class Magestore_Affiliateplus_Model_Total_Address_Tax extends Mage_Tax_Model_Sales_Total_Quote_Tax
{
    protected function _aggregateTaxPerRate($item, $rate, &$taxGroups) {
        $discount       = $item->getDiscountAmount();
        $baseDiscount   = $item->getBaseDiscountAmount();
        $item->setDiscountAmount($discount+$item->getAffiliateplusAmount());
        $item->setBaseDiscountAmount($baseDiscount+$item->getBaseAffiliateplusAmount());
        
        parent::_aggregateTaxPerRate($item, $rate, $taxGroups);
        
        $item->setDiscountAmount($discount);
        $item->setBaseDiscountAmount($baseDiscount);
        return $this;
    }
    
    protected function _calcUnitTaxAmount(Mage_Sales_Model_Quote_Item_Abstract $item, $rate) {
        $discount       = $item->getDiscountAmount();
        $baseDiscount   = $item->getBaseDiscountAmount();
        $item->setDiscountAmount($discount+$item->getAffiliateplusAmount());
        $item->setBaseDiscountAmount($baseDiscount+$item->getBaseAffiliateplusAmount());
        
        parent::_calcUnitTaxAmount($item, $rate);
        
        $item->setDiscountAmount($discount);
        $item->setBaseDiscountAmount($baseDiscount);
        return $this;
    }
    
    protected function _calcRowTaxAmount($item, $rate) {
        $discount       = $item->getDiscountAmount();
        $baseDiscount   = $item->getBaseDiscountAmount();
        $item->setDiscountAmount($discount+$item->getAffiliateplusAmount());
        $item->setBaseDiscountAmount($baseDiscount+$item->getBaseAffiliateplusAmount());
        
        parent::_calcRowTaxAmount($item, $rate);
        
        $item->setDiscountAmount($discount);
        $item->setBaseDiscountAmount($baseDiscount);
        return $this;
    }
}
