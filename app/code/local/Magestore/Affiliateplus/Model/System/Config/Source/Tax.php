<?php

class Magestore_Affiliateplus_Model_System_Config_Source_Tax
{
    /**
     * Options getter
     *
     * @return array
     */
     public function toOptionArray(){

        return array(

			array('value' => '1', 'label'=>Mage::helper('affiliateplus')->__('Calculate Discount Incl Tax')),

            array('value' => '0', 'label'=>Mage::helper('affiliateplus')->__('Calculate Discount Excl Tax')),

        );

    }
}