<?php

class Magestore_Affiliateplus_Model_Quote extends Mage_Core_Model_Abstract
{	
    public function _construct(){
        parent::_construct();
        $this->_init('affiliateplus/quote');
    }
	
	
	public function getAffiliateQuote($quoteId) {
		if($quoteId) {
			$collection = $this->getCollection()
						->addFieldToFilter('quote_id', $quoteId)
						;
			foreach($collection as $item) {
				if($item && $item->getId()) return $item;
			}
		}
		return null;
	}
}