<?php
class Magestore_Faq_Block_Sidebar extends Mage_Core_Block_Template
{
	protected function returnSidebar($sidebarposition)
	{
		$checkposition = Mage::getStoreConfig('faq/general/sidebar_position');
		$sidebar = $this->getLayout()->createBlock('faq/sidebar')->setTemplate('faq/sidebar.phtml')->renderView();
		if ($sidebarposition == $checkposition) return $sidebar;
	}
	
	protected function _toHtml()
    {
		return $this->returnSidebar($this->getSidebarPosition());
	}
	
	public function getMostFrequently()
	{
		$most_frequently = Mage::getModel("faq/faq")
								->setStoreId($this->getStoreId())
								->getMostFrequently();
		
		return $most_frequently;
	}
                //phudu25
        public function getStoreId() {
        if (!$this->hasData('store_id')) {
            $store_id = Mage::app()->getStore()->getId();
            $this->setData('store_id', $store_id);
        }
        $categories = Mage::getModel("faq/category")
                ->setStoreId($this->getData('store_id'))
                ->getCollection()
        ;
        if($categories->getSize()==0){
            $this->setData('store_id', 0);
        }
        return $this->getData('store_id');
    }
}