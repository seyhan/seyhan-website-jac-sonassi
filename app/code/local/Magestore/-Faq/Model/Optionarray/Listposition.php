<?php

class Magestore_Faq_Model_Optionarray_Listposition
{
	/**/
    public function toOptionArray()
    {
		$positionarray = array(
								'no-display' => Mage::helper('faq')->__('No display'),								
								'sidebar-right' => Mage::helper('faq')->__('Diplay on right sidebar'),
								'sidebar-left' => Mage::helper('faq')->__('Diplay on left sidebar'),
							);
		
		return $positionarray;
    }
}