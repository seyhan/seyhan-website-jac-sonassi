<?php

class Magestore_Faq_Model_Observer{
    public function afterSaveStore($observer){
        $store=$observer->getEvent()->getStore();
        //Zend_Debug::dump($store->isObjectNew());die("1");
        if($store->isObjectNew()){
            $store_id=$store->getId();
            $cate_ids=Mage::getModel('faq/category')->getCollection()->getAllIds();
            //Zend_Debug::dump($cate_ids);die("1");
            if(count($cate_ids)){
                foreach ($cate_ids as $catId){
                    $categoryStore = Mage::getModel("faq/categorystore")->loadByCatIdStore($catId,0);
                    $catStore=Mage::getModel('faq/categorystore')->setData($categoryStore->getData())
                            ->setStoreId($store_id)
                            ->setId(null)
                            ->save();
                }
            }
            //return;
            $faq_ids=Mage::getModel('faq/faq')->getCollection()->getAllIds();
            //Zend_Debug::dump($faq_ids);die("1");
            if(count($faq_ids)){
                foreach ($faq_ids as $faqId){
                    $faqStoreDefault = Mage::getModel("faq/faqstore")->loadByFaqIdStore($faqId,0);
                    $faqStore=Mage::getModel('faq/faqstore')->setData($faqStoreDefault->getData())
                            ->setStoreId($store_id)
                            ->setId(null)
                            ->save();
            }
            }
        }
    }
}