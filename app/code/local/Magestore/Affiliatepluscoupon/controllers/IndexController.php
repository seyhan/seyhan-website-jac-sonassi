<?php

class Magestore_Affiliatepluscoupon_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){
		if (Mage::helper('affiliatepluscoupon')->couponIsDisable())
			return $this->_redirect('affiliateplus/index/index');
		$account = Mage::helper('affiliateplus/account')->getAccount();
		if ($account->getCouponCode()){
			Mage::register('account_model',$account);
		} else {
			$accountModel = Mage::getModel('affiliateplus/account')->load($account->getId());
			$code = $this->getRequest()->getPost('your-code');				
			$check = Mage::helper('affiliatepluscoupon')->checkCodeExist($code);
			if($code){
				if($check == 0) {
					$message = Mage::helper('affiliateplus')->__('Coupon Code already exist!');
					Mage::getSingleton('core/session')->addError($message); 	
				}				
				else{				
					try{									
						$accountModel->setCouponCode($code);					
						$accountModel->save();					
					}catch (Exception $e){
						
					}	
				}		
			}
			Mage::register('account_model',$accountModel);
						
		}
		$this->loadLayout();
		$this->getLayout()->getBlock('head')->setTitle($this->__('Coupon Code'));
		$this->renderLayout();
	}
}