<?php

class Magestore_Affiliatepluscoupon_Model_Observer
{
	public function couponPostAction($observer){
		if (!Mage::getStoreConfig('affiliateplus/coupon/enable')) return $this;
		
		$action = $observer->getEvent()->getControllerAction();
		$code = trim($action->getRequest()->getParam('coupon_code'));
		if (!$code) return $this;
		
		$session = Mage::getSingleton('checkout/session');
		
		$account = Mage::getModel('affiliateplus/account')->load($code,'coupon_code');
		$customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
		if (!$account->getId() || $account->getCustomerId() == $customerId){
			$session->unsetData('affiliate_coupon_code');
			$this->clearAffiliateCookie();
			return $this;
		}
		
		if ($action->getRequest()->getParam('remove') == 1){
			if ($account->getCouponCode() == $session->getData('affiliate_coupon_code')){
				$session->unsetData('affiliate_coupon_code');
				$this->clearAffiliateCookie();
				$session->addSuccess(Mage::helper('affiliatepluscoupon')->__('Coupon code was canceled.'));
			}
		} else {
			$session->setData('affiliate_coupon_code',$account->getCouponCode());
			$this->clearAffiliateCookie();
			Mage::getSingleton('core/cookie')->set('affiliateplus_map_index',1);
			Mage::getSingleton('core/cookie')->set('affiliateplus_account_code_1',$account->getIdentifyCode());
			$session->addSuccess(Mage::helper('affiliatepluscoupon')->__('Coupon code "%s" was applied.',$code));
		}
		$action->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
		$action->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));
	}
	
	public function clearAffiliateCookie(){
		$cookie = Mage::getSingleton('core/cookie');
		for ($index = intval($cookie->get('affiliateplus_map_index')); $index>0; $index--)
			$cookie->delete("affiliateplus_account_code_$index");
		$cookie->delete('affiliateplus_map_index');
		return $this;
	}
}