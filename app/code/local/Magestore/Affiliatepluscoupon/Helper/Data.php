<?php

class Magestore_Affiliatepluscoupon_Helper_Data extends Mage_Core_Helper_Data
{
	public function calcCode($expression){
		if ($this->isExpression($expression)){
			return preg_replace_callback('#\[([AN]{1,2})\.([0-9]+)\]#',array($this,'convertExpression'),$expression);
		}else{
			return $expression;
		}
	}
	
	public function convertExpression($param){
		$alphabet  = (strpos($param[1],'A'))===false ? '':'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$alphabet .= (strpos($param[1],'N'))===false ? '': '0123456789';
		return $this->getRandomString($param[2],$alphabet);
	}
	
	public function isExpression($string){
		return preg_match('#\[([AN]{1,2})\.([0-9]+)\]#',$string);
	}
	
	public function generateNewCoupon(){
		$expression = Mage::getStoreConfig('affiliateplus/coupon/pattern');
		if (!$this->isExpression())
			$expression = 'AFFILIATE-[N.4]-[AN.5]-[A.4]';
		$code = $this->calcCode($expression);
		$times = 10;
		while (Mage::getModel('affiliateplus/account')->load($code,'coupon_code')->getId() && $times){
			$times--;
			if ($times == 0){
				throw new Exception($this->__('Tried to generate coupon code 10 times!'));
			} else {
				$code = $this->calcCode($expression);
			}
		}
		return $code;
	}
	
	public function couponIsDisable(){
		return !Mage::getStoreConfig('affiliateplus/coupon/enable') || Mage::helper('affiliateplus/account')->accountNotLogin();
	}
	
	public function checkCodeExist($code){
		$accounts = Mage::getModel('affiliateplus/account')->getCollection();
		$check = 1;
		foreach($accounts as $acc){
			if($code == $acc->getCouponCode()) {$check = 0; break;}
		}
		return $check;
	}
}