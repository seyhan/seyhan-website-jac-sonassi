<?php
/**
 * Morningtime LatestReviews extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Morningtime
 * @package    Morningtime_LatestReviews
 * @copyright  Copyright (c) 2009 Morningtime Internet, http://www.morningtime.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Morningtime_LatestReviews_Block_List extends Mage_Review_Block_View
{
    protected $_defaultToolbarBlock = 'latestreviews/list_toolbar';	
	
    public function getReviewsCollection()
    {
        $collection = Mage::getResourceModel('review/review_collection')
            ->addStoreFilter(Mage::app()->getStore()->getStoreId())
            ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
            ->setDateOrder()
            ->setPageSize(6)
            ->setCurPage(1)
            ->load();
        $collection->addRateVotes();

        return $collection;
    }

    public function dateFormat($date)
    {
        return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
    }

    public function getReviewLink($id)
    {
        return Mage::getUrl('review/product/view', array('id' => $id));
    }
	
	public function getListFields()
	{
		return Mage::getStoreConfig('latestreviews/reviews/show_fields');
	}
	
	public function getListLimit()
	{
		return intval(Mage::getStoreConfig('latestreviews/general/num_displayed_reviews'));
	}

    public function getProductLink($p, $r)
    {
    	$linktype = Mage::getStoreConfig('latestreviews/reviews/link_to');
		switch ($linktype)
		{
		case 'product':
			$u = $p->getProductUrl();
		break;
		case 'review':
			$u = $this->getReviewLink($r['review_id']);
		break;
		case 'listing':
			$c = $p->getCategoryIds();
			$u = Mage::getUrl('review/product/list/id/', array('id' => $r['entity_pk_value']))."category/".$c[0]."/";
		break;
		case 'section':
			$c = $p->getCategoryIds();
			$u = Mage::getUrl('review/product/list/id/', array('id' => $r['entity_pk_value']))."category/".$c[0]."#customer-reviews";
		break;
		default:
			$u = $p->getProductUrl();
		}
        return $u;
    }
	
    public function getTitle($p, $r)
    {
    	$showtitle = Mage::getStoreConfig('latestreviews/reviews/show_title');
		switch ($showtitle)
		{
		case 'product':
			$t = $p->getName();
		break;
		case 'review':
			$t = $r['title'];
		break;
		default:
			$t = $p->getName();
		}
        return $this->htmlEscape($t);
    }

    /**
     * Translate block sentence
     *
     * @return string
     */
    public function __()
    {
        $args = func_get_args();
        $expr = new Mage_Core_Model_Translate_Expr(array_shift($args), 'Mage_Catalog');
        array_unshift($args, $expr);
        return Mage::app()->getTranslator()->translate($args);
    }

}
